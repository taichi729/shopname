//
//  ViewController.swift
//  IOS9DrawRouteMapKitTutorial
//
//  Created by Arthur Knopper on 09/02/16.
//  Copyright © 2016 Arthur Knopper. All rights reserved.
//

import UIKit
import MapKit

class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
}
class ViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.frame = view.frame
        
        // 1.
        mapView.delegate = self
        mapView.isPitchEnabled = false
//        mapView.isZoomEnabled = false
        
        mapView.showsPointsOfInterest = false
        mapView.backgroundColor = .white
//        let gess = UIPinchGestureRecognizer(target:self , action: #selector(zooming(_:)))
//        mapView.addGestureRecognizer(gess)
     
        // 2.
        let sourceLocation = CLLocationCoordinate2D(latitude: 7.883539, longitude: 98.387496)
        let destinationLocation = CLLocationCoordinate2D(latitude: 7.884539, longitude: 98.387496)
        var mkcamera  = MKMapCamera(lookingAtCenter: sourceLocation,
                                    fromDistance: CLLocationDistance(100),
                                    pitch: 75,
                                    heading: 0)
        mapView.setCamera(mkcamera, animated: true)
        print(mapView.camera.altitude,"kk")
        // 3.
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
     
        // 4.
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        // 5.
        let sourceAnnotation = CustomPointAnnotation()
        sourceAnnotation.title = "Times Square"
        sourceAnnotation.subtitle = "1"
//        sourceAnnotation.imageName = "dog-paw"
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = "Empire State Building"
        
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        // 6.
        self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        self.mapView.selectAnnotation(sourceAnnotation, animated: false)
        
//        // 7.
//        let directionRequest = MKDirectionsRequest()
//        directionRequest.source = sourceMapItem
//        directionRequest.destination = destinationMapItem
//        directionRequest.transportType = .automobile
//        
//        // Calculate the direction
//        let directions = MKDirections(request: directionRequest)
//        
//        // 8.
//        directions.calculate {
//            (response, error) -> Void in
//            
//            guard let response = response else {
//                if let error = error {
//                    print("Error: \(error)")
//                }
//                
//                return
//            }
//            
//            let route = response.routes[0]
//            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
//            
//            let rect = route.polyline.boundingMapRect
//            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
//        }
       
    }
//    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        // simple and inefficient example
//        if !(annotation is CustomPointAnnotation) {
//            return nil
//        }
//        
//        let reuseId = "test"
//        
//        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
//        if anView == nil {
//            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
//            anView?.canShowCallout = true
//        }
//        else {
//            anView?.annotation = annotation
//        }
//        
//        //Set annotation-specific properties
//        
//        //the view is dequeued or created...
//        
//        let cpa = annotation as! CustomPointAnnotation
//        print(anView?.image?.size)
//        anView?.image = UIImage(named:cpa.imageName)
//        
//        return anView
//    }
let maxzoom = 3000
    let minzoom = 100
//    
//    func zooming(_ pinch : UIPinchGestureRecognizer){
//    print(pinch.scale)
//        let mapcam = mapView.camera
//        var increse = CLLocationDistance()
//        let s = pinch.scale/4
//        let sd = (pinch.scale)
//        let zoom = CLLocationDistance(300)
//        if pinch.scale > 1 {increse = CLLocationDistance(s);mapcam.altitude -= CLLocationDistance(zoom*increse)}else if pinch.scale<1 {increse = CLLocationDistance(sd);mapcam.altitude += CLLocationDistance(zoom*increse)}
//        
//        if mapcam.altitude > 3000 {
////        mapcam.pitch = 0
//            provincegroup()
//        }
//    
//        pinch.scale = 1
//
//    }
    func provincegroup() {
        var points=[CLLocationCoordinate2DMake(7.776318, 98.289231)
            ,CLLocationCoordinate2DMake(7.782398, 98.288823)
            ,CLLocationCoordinate2DMake(7.784439, 98.291398)
            ,CLLocationCoordinate2DMake(7.788096, 98.291484)
            ,CLLocationCoordinate2DMake(7.788011, 98.285476)
            ,CLLocationCoordinate2DMake(7.791540, 98.285626)
            ,CLLocationCoordinate2DMake(7.793241, 98.293737)
            ,CLLocationCoordinate2DMake(7.802722, 98.299445)
            ,CLLocationCoordinate2DMake(7.809653, 98.298844)
            ,CLLocationCoordinate2DMake(7.810684, 98.296269)
            ,CLLocationCoordinate2DMake(7.813299, 98.296398)
            ,CLLocationCoordinate2DMake(7.814670, 98.300024)
            ,CLLocationCoordinate2DMake(7.824789, 98.293050)
            ,CLLocationCoordinate2DMake(7.829168, 98.294724)
            ,CLLocationCoordinate2DMake(7.859310, 98.289767)
            ,CLLocationCoordinate2DMake(7.860118, 98.283502)
            ,CLLocationCoordinate2DMake(7.859905, 98.283287)
            ,CLLocationCoordinate2DMake(7.864050, 98.284210)
            ,CLLocationCoordinate2DMake(7.865994, 98.277756)
            ,CLLocationCoordinate2DMake(7.869225, 98.274151)
            ,CLLocationCoordinate2DMake(7.874667, 98.276640)
            ,CLLocationCoordinate2DMake(7.869916, 98.298994)
            ,CLLocationCoordinate2DMake(7.868715, 98.310887)
            ,CLLocationCoordinate2DMake(7.874667, 98.276640)
            ,CLLocationCoordinate2DMake(7.830103, 98.318349)
            ,CLLocationCoordinate2DMake(7.825097, 98.319239)
            ,CLLocationCoordinate2DMake(7.817954, 98.319239)
            ,CLLocationCoordinate2DMake(7.810131, 98.311515)
            ,CLLocationCoordinate2DMake(7.797716, 98.307652)
            ,CLLocationCoordinate2DMake(7.795505, 98.312287)
            ,CLLocationCoordinate2DMake(7.788893, 98.301075)
            ,CLLocationCoordinate2DMake(7.778178, 98.299874)]
        
        let polygon = MKPolygon(coordinates: &points, count: points.count)
        
        mapView.add(polygon)
       
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolygonRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        renderer.fillColor = .magenta
        
        return renderer
    }
    var savecam = CGFloat()
    var lastheight = CLLocationDistance()
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let theata = mapView.camera.pitch
        let height = mapView.camera.altitude
        let mapcam = mapView.camera
        let annotations = self.mapView.annotations
        
        for annotation in annotations
        {
            if (mapcam.altitude > 2700)
            {
                
                self.mapView.view(for: annotation)?.isHidden = true
                
            }
            else {
                
                self.mapView.view(for: annotation)?.isHidden = false
                
            }
        }
print(CGFloat(mapcam.altitude),"print")
        let sss = CGFloat(90-CGFloat(mapcam.altitude)/CGFloat(30))
        
        if mapcam.altitude < 2700 && mapcam.altitude != lastheight{
            print(CGFloat(mapcam.altitude),"print")
            print(mapcam.pitch,"happy")
            lastheight = mapView.camera.altitude
//            mapcam.pitch = CGFloat(30)
            mapcam.pitch = sss
           
            
          
            savecam = theata
            }
        
            if mapcam.pitch > 80 {
                mapcam.pitch = 80
                savecam = theata
            }
        if mapcam.altitude > 2700 && mapcam.pitch != 0 {
        mapcam.pitch = 0
        
        }
        if mapcam.altitude > 3000 {
            //        mapcam.pitch = 0
            provincegroup()
        }
        }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

