import UIKit
import FirebaseStorage
import FirebaseAuth
import FirebaseCore
import FirebaseDatabase


import Foundation

class Couponedit : UIViewController/*,UITableViewDelegate,UITableViewDataSource */{
    let Shopname = UILabel()
    let hour = UILabel()
    let Coupon = UITableView()
    var shops = Shop()
    var shopImageHeight:CGFloat?
    var shopImageWidth:CGFloat?
    
    var recommendImageWidth1:CGFloat?
    var recommendImageWidth2:CGFloat?
    var recommendImageHeight1:CGFloat?
    var recommendImageHeight2:CGFloat?
    
    var isHaveRecommendMenu:Bool?
    
    let shopImage = UIImageView()
    let loveImage = UIButton()
    let nameShop = UILabel()
    let ratingImage = UIImageView()
    
    let highlight = UILabel()
    
    let menu = UIImageView()
    
    let youMayLike = UILabel()
    
    let recommendMenu1 = UIImageView()
    let itemMenu1 = UILabel()
    let priceMenu1 = UILabel()
    
    let recommendMenu2 = UIImageView()
    let itemMenu2 = UILabel()
    let priceMenu2 = UILabel()
    
    let imageShopFront = UIImageView()
    let imageShopIn = UIImageView()
    let imageShopCool = UIImageView()
    
    let call = UIButton()
    let map = UIButton()
    let time = UILabel()
    let viewcontroller = UIScrollView()
    override func viewDidLoad() {
        viewcontroller.alwaysBounceVertical = true
        viewcontroller.bouncesZoom = false
        viewcontroller.showsHorizontalScrollIndicator = false
        viewcontroller.scrollsToTop = true
        viewcontroller.frame = CGRect(x:0,y:0,width:self.view.frame.width,height:self.view.frame.height
        )
        view.addSubview(viewcontroller)
        if shops.isFavorite == true {
            self.loveImage.setImage(UIImage(named: "like"), for: .normal)
        } else { self.loveImage.setImage(UIImage(named: "like-black"), for: .normal) }
               self.nameShop.text = shops.shop
        
        self.ratingImage.image = nil
        if let x = shops.rateTotal {
            let rate = x as Int
            if rate < 1 {
                self.ratingImage.image = #imageLiteral(resourceName: "dissapointment-2")
            } else if rate < 2 {
                self.ratingImage.image = #imageLiteral(resourceName: "confused-2")
            } else if rate < 3 {
                self.ratingImage.image = #imageLiteral(resourceName: "wink-2")
            } else {
                self.ratingImage.image = #imageLiteral(resourceName: "flirt-2")
            }
        }
        
        self.highlight.text = shops.descripTion
        
        self.menu.image = nil
        let menu_image = FIRStorage.storage().reference(withPath: "shop_images/\(shops.shop!)/menu.jpg")
        if let cachedImage = imageCache.object(forKey: menu_image as AnyObject) as? UIImage {
            self.menu.image = cachedImage
        } else {
            menu_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                self.menu.alpha = 0
                self.menu.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    self.menu.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: menu_image as AnyObject)
            })
        }

        let activity_image = FIRStorage.storage().reference(withPath: "shop_images/\(shops.shop!)/activity.jpg")
        if let cachedImage = imageCache.object(forKey: activity_image as AnyObject) as? UIImage {
            shopImage.image = cachedImage
        } else {
            activity_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                self.shopImage.alpha = 0
                self.shopImage.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    self.shopImage.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: activity_image as AnyObject)
            })
        }
      shopImageHeight = shops.heightImage as! CGFloat
       shopImageWidth =  shops.widthImage  as! CGFloat
         recommendImageHeight1 = shops.heightImageItem1 as CGFloat?
         recommendImageWidth1 = shops.widthImageItem1 as CGFloat?
        recommendImageHeight2 = shops.heightImageItem2 as CGFloat?
        recommendImageWidth2 = shops.widthImageItem2 as CGFloat?
       
        
        
        self.recommendMenu1.image = nil
        let recommendMenu1_image = FIRStorage.storage().reference(withPath: "shop_images/\(shops.shop!)/recommendMenu1.jpg")
        if let cachedImage = imageCache.object(forKey: recommendMenu1_image as AnyObject) as? UIImage {
            self.recommendMenu1.image = cachedImage
        } else {
            recommendMenu1_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                self.recommendMenu1.alpha = 0
                self.recommendMenu1.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    self.recommendMenu1.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: recommendMenu1_image as AnyObject)
            })
        }
        
        self.recommendMenu2.image = nil
        let recommendMenu2_image = FIRStorage.storage().reference(withPath: "shop_images/\(shops.shop!)/recommendMenu2.jpg")
        if let cachedImage = imageCache.object(forKey: recommendMenu2_image as AnyObject) as? UIImage {
            self.recommendMenu2.image = cachedImage
        } else {
            recommendMenu2_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                self.recommendMenu2.alpha = 0
                self.recommendMenu2.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    self.recommendMenu2.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: recommendMenu2_image as AnyObject)
            })
        }
        
        self.recommendImageWidth1 = shops.widthImageItem1 as CGFloat?
        self.recommendImageWidth2 = shops.widthImageItem2 as CGFloat?
        self.recommendImageHeight1 = shops.heightImageItem1 as CGFloat?
        self.recommendImageHeight2 = shops.heightImageItem2 as CGFloat?
        if shops.menuItem1 == nil {
            self.isHaveRecommendMenu = false
        } else {
            self.isHaveRecommendMenu = true
            
            self.recommendMenu1.image = nil
            let recommend1_image = FIRStorage.storage().reference(withPath: "shop_images/\(shops.shop!)/\(shops.menuItem1!).jpg")
            if let cachedImage = imageCache.object(forKey: recommend1_image as AnyObject) as? UIImage {
                self.recommendMenu1.image = cachedImage
            } else {
                recommend1_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                    if err != nil {
                        print(err!)
                        return
                    }
                    
                    self.recommendMenu1.alpha = 0
                    self.recommendMenu1.image = UIImage(data: data!)
                    UIView.animate(withDuration: 0.5, animations: {
                        self.recommendMenu1.alpha = 1
                    }, completion: nil)
                    imageCache.setObject(UIImage(data: data!)!, forKey: recommend1_image as AnyObject)
                })
            }
            
            self.recommendMenu2.image = nil
            let recommend2_image = FIRStorage.storage().reference(withPath: "shop_images/\(shops.shop!)/\(shops.menuItem2!).jpg")
            if let cachedImage = imageCache.object(forKey: recommend2_image as AnyObject) as? UIImage {
                self.recommendMenu2.image = cachedImage
            } else {
                recommend2_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                    if err != nil {
                        print(err!)
                        return
                    }
                    
                    self.recommendMenu2.alpha = 0
                    self.recommendMenu2.image = UIImage(data: data!)
                    UIView.animate(withDuration: 0.5, animations: {
                        self.recommendMenu2.alpha = 1
                    }, completion: nil)
                    imageCache.setObject(UIImage(data: data!)!, forKey: recommend2_image as AnyObject)
                })
            }
            
        }
        
        self.itemMenu1.text = shops.menuItem1 //"coco"
        self.itemMenu2.text = shops.menuItem2 //"salad"
        
        self.priceMenu1.text = shops.priceItem1 //"50 Bath"
        self.priceMenu2.text = shops.priceItem2 //"75 Bath"
        
        self.imageShopFront.image = nil
        let shopFront_image = FIRStorage.storage().reference(withPath: "shop_images/\(shops.shop!)/shopFront.jpg")
        if let cachedImage = imageCache.object(forKey: shopFront_image as AnyObject) as? UIImage {
            self.imageShopFront.image = cachedImage
        } else {
            shopFront_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                self.imageShopFront.alpha = 0
                self.imageShopFront.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    self.imageShopFront.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: shopFront_image as AnyObject)
            })
        }
        
        self.imageShopIn.image = nil
        let shopIn_image = FIRStorage.storage().reference(withPath: "shop_images/\(shops.shop!)/shopIn.jpg")
        if let cachedImage = imageCache.object(forKey: shopIn_image as AnyObject) as? UIImage {
            self.imageShopIn.image = cachedImage
        } else {
            shopIn_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                self.imageShopIn.alpha = 0
                self.imageShopIn.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    self.imageShopIn.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: shopIn_image as AnyObject)
            })
        }
        
        self.imageShopCool.image = nil
        let shopCool_image = FIRStorage.storage().reference(withPath: "shop_images/\(shops.shop!)/shopCool.jpg")
        if let cachedImage = imageCache.object(forKey: shopCool_image as AnyObject) as? UIImage {
            self.imageShopCool.image = cachedImage
        } else {
            shopCool_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                self.imageShopCool.alpha = 0
                self.imageShopCool.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    self.imageShopCool.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: shopCool_image as AnyObject)
            })
        }
        
        if shops.phone != nil {
//            self.call.addTarget(self, action: #selector(callPhone), for: .touchUpInside)
        }
        
//        self.map.addTarget(self, action: #selector(directionMap), for: .touchUpInside)
        
        if let open = shops.TimeOpened , let close = shops.TimeClosed {
            
            let currentTime = (Float(NSDate().timeIntervalSince1970)).truncatingRemainder(dividingBy: 86400)
            
            let timeOpen = open as Float
            let timeopenHour = timeOpen/3600
            let timeopenMinute = (timeOpen.truncatingRemainder(dividingBy: 3600))/60
            
            let timeClose = close as Float
            let timeCloseHour = timeClose/3600
            let timeCloseMinute = (timeClose.truncatingRemainder(dividingBy: 3600))/60
            
            print(currentTime,timeOpen,timeClose)
            if currentTime < timeOpen || currentTime > timeClose {
                self.time.text = "Close"
                self.time.textColor = .red
            } else {
                self.time.text = "Open \(Int(timeopenHour)):\(Int(timeopenMinute)) - \(Int(timeCloseHour)):\(Int(timeCloseMinute))"
                self.time.textColor = .green
            }
        
        }
        
        shopImage.backgroundColor = .gray
        menu.backgroundColor = .gray
        recommendMenu1.backgroundColor = .gray
        recommendMenu2.backgroundColor = .gray
        imageShopCool.backgroundColor = .gray
        imageShopIn.backgroundColor = .gray
        imageShopFront.backgroundColor = .gray
        shopImage.frame = CGRect(x: 0, y: 0, width: self.viewcontroller.frame.width, height: self.viewcontroller.frame.width * shopImageHeight!/shopImageWidth!)
        viewcontroller.addSubview(shopImage)
        
        loveImage.frame = CGRect(x: 7, y: shopImage.frame.maxY + 6, width: self.viewcontroller.frame.width/14, height: self.viewcontroller.frame.width/14)
        viewcontroller.addSubview(loveImage)
        
        ratingImage.frame.size.width = loveImage.frame.width
        ratingImage.frame.size.height = loveImage.frame.height
        ratingImage.frame.origin.x = self.viewcontroller.frame.width - ratingImage.frame.width - 7
        ratingImage.frame.origin.y = loveImage.frame.origin.y
        viewcontroller.addSubview(ratingImage)
        
        nameShop.frame = CGRect(x: loveImage.frame.maxX + 2, y: loveImage.frame.origin.y, width: ratingImage.frame.origin.x - loveImage.frame.maxX - 2, height: ratingImage.frame.height)
        viewcontroller.addSubview(nameShop)
        nameShop.textAlignment = .center
        nameShop.font = UIFont(name: "Oxygen-Bold", size: 15)
        nameShop.adjustsFontSizeToFitWidth = true
        
        highlight.frame = CGRect(x: loveImage.frame.origin.x, y: nameShop.frame.maxY + 15, width: self.viewcontroller.frame.width - loveImage.frame.origin.x*2, height: nameShop.frame.height)
//        viewcontroller.addSubview(highlight)
        highlight.font = UIFont(name: "Oxygen-Regular", size: 12)
        highlight.textAlignment = .left
        highlight.numberOfLines = 10
        highlight.frame.size.height = estimatedsize(text: highlight.text!, sizee: 12, font: "Oxygen-Regular", width: highlight.frame.width).height
        
        menu.frame = CGRect(x: 0, y: highlight.frame.maxY + 18, width: self.viewcontroller.frame.width, height: self.viewcontroller.frame.width * (1920/1080))
//        viewcontroller.addSubview(menu)
        
        if isHaveRecommendMenu == false {
            youMayLike.frame = CGRect(x: highlight.frame.origin.x + 3, y: menu.frame.maxY + 4, width: self.viewcontroller.frame.width - highlight.frame.origin.x, height: 0)
            
            recommendMenu1.frame = CGRect(x: highlight.frame.origin.x, y: youMayLike.frame.maxY + 5, width: self.viewcontroller.frame.width/2 - 4*2, height: 0)
            
            recommendMenu2.frame = CGRect(x: recommendMenu1.frame.maxX + 8, y: recommendMenu1.frame.origin.y, width: self.viewcontroller.frame.width/2 - 4*2, height: 0)
            
            itemMenu1.frame = CGRect(x: recommendMenu1.frame.origin.x + 1, y: recommendMenu1.frame.maxY + 3, width: recommendMenu1.frame.width/1.5, height: 0 )
            
            itemMenu2.frame = CGRect(x: recommendMenu2.frame.origin.x + 1, y: recommendMenu2.frame.maxY + 3, width: recommendMenu2.frame.width/1.5, height: 0)
            
            priceMenu1.frame = CGRect(x: itemMenu1.frame.maxX + 4, y: itemMenu1.frame.origin.y + 6, width: recommendMenu1.frame.width - itemMenu1.frame.width, height: 0)
            
            priceMenu2.frame = CGRect(x: itemMenu2.frame.maxX + 4, y: itemMenu2.frame.origin.y + 6, width: recommendMenu2.frame.width - itemMenu2.frame.width, height: 0)
        } else {
            
            youMayLike.frame = CGRect(x: highlight.frame.origin.x + 3, y: menu.frame.maxY + 4, width: self.viewcontroller.frame.width - highlight.frame.origin.x, height: loveImage.frame.height)
//            viewcontroller.addSubview(youMayLike)
            youMayLike.text = "You may like"
            youMayLike.font = UIFont(name: "Oxygen-Bold", size: 15)
            
            recommendMenu1.frame = CGRect(x: highlight.frame.origin.x, y: youMayLike.frame.maxY + 5, width: self.viewcontroller.frame.width/2 - 4*2, height: (self.viewcontroller.frame.width/2 - 4*2) * (600/400))
//            viewcontroller.addSubview(recommendMenu1)
            
            itemMenu1.frame = CGRect(x: recommendMenu1.frame.origin.x + 1, y: recommendMenu1.frame.maxY + 3, width: recommendMenu1.frame.width/1.5, height: youMayLike.frame.height)
//            viewcontroller.addSubview(itemMenu1)
            itemMenu1.font = UIFont(name: "Oxygen-Regular", size: 14)
            let itemMenu1_size:CGSize = (itemMenu1.text! as NSString).size(attributes: [NSFontAttributeName: UIFont(name: "Oxygen-Regular", size: 14)!])
            if itemMenu1.frame.width <= itemMenu1_size.width {
                itemMenu1.adjustsFontSizeToFitWidth = true
            } else {
                itemMenu1.frame.size.width = itemMenu1_size.width
            }
            
            priceMenu1.frame = CGRect(x: itemMenu1.frame.maxX + 4, y: itemMenu1.frame.origin.y + 6, width: recommendMenu1.frame.width - itemMenu1.frame.width, height: itemMenu1.frame.height)
//            viewcontroller.addSubview(priceMenu1)
            priceMenu1.font = UIFont(name: "Oxygen-Regular", size: 14)
            priceMenu1.adjustsFontSizeToFitWidth = true
            
            recommendMenu2.frame = CGRect(x: recommendMenu1.frame.maxX + 8, y: recommendMenu1.frame.origin.y, width: self.viewcontroller.frame.width/2 - 4*2, height: (self.viewcontroller.frame.width/2 - 4*2) * (600/350))
//            viewcontroller.addSubview(recommendMenu2)
            
            itemMenu2.frame = CGRect(x: recommendMenu2.frame.origin.x + 1, y: recommendMenu2.frame.maxY + 3, width: recommendMenu2.frame.width/1.5, height: youMayLike.frame.height)
//            viewcontroller.addSubview(itemMenu2)
            itemMenu2.font = UIFont(name: "Oxygen-Regular", size: 14)
            let itemMenu2_size:CGSize = (itemMenu2.text! as NSString).size(attributes: [NSFontAttributeName: UIFont(name: "Oxygen-Regular", size: 14)!])
            if itemMenu2.frame.width <= itemMenu2_size.width {
                itemMenu2.adjustsFontSizeToFitWidth = true
            } else {
                itemMenu2.frame.size.width = itemMenu2_size.width
            }
            
            priceMenu2.frame = CGRect(x: itemMenu2.frame.maxX + 4, y: itemMenu2.frame.origin.y + 6, width: recommendMenu2.frame.width - itemMenu2.frame.width, height: itemMenu2.frame.height)
//            viewcontroller.addSubview(priceMenu2)
            priceMenu2.font = UIFont(name: "Oxygen-Regular", size: 14)
            priceMenu2.adjustsFontSizeToFitWidth = true
        }
        
        imageShopFront.frame = CGRect(x: 0, y: itemMenu2.frame.maxY + 35, width: self.viewcontroller.frame.width, height: self.viewcontroller.frame.width * (1080/1920))
//        viewcontroller.addSubview(imageShopFront)
        
        if recommendMenu1.frame.height > recommendMenu2.frame.height {
            imageShopFront.frame.origin.y = itemMenu1.frame.maxY + 35
        }
        
        imageShopIn.frame = CGRect(x: 0, y: imageShopFront.frame.maxY + 7, width: self.viewcontroller.frame.width, height: self.viewcontroller.frame.width * (1080/1920))
//        viewcontroller.addSubview(imageShopIn)
        
        imageShopCool.frame = CGRect(x: 0, y: imageShopIn.frame.maxY + 7, width: self.viewcontroller.frame.width, height: self.viewcontroller.frame.width * (1080/1920))
//        viewcontroller.addSubview(imageShopCool)
        
        call.frame = CGRect(x: 4, y: imageShopCool.frame.maxY + 5, width: loveImage.frame.width, height: loveImage.frame.width)
//        viewcontroller.addSubview(call)
        call.setImage(#imageLiteral(resourceName: "phone"), for: .normal)
        
        map.frame = CGRect(x: call.frame.maxX + 15, y: call.frame.origin.y, width: call.frame.width, height: call.frame.height)
//        viewcontroller.addSubview(map)
        map.setImage(#imageLiteral(resourceName: "location"), for: .normal)
        
        time.frame = CGRect(x: map.frame.maxX + 3, y:map.frame.origin.y, width: self.viewcontroller.frame.width - (map.frame.maxX + 10), height:map.frame.height)
//        viewcontroller.addSubview(time)
        time.font = UIFont(name: "Oxygen-Bold", size: 15)
        time.textAlignment = .right
    }
    
   
    

    func troll() {
        
        let navigationItem = UINavigationItem()
        navigationItem.title = "Profile"
        let rightButton = UIBarButtonItem(title: "Edit", style: UIBarButtonItemStyle.plain, target: self, action: #selector(editing))
        
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title:"<Back",style:   UIBarButtonItemStyle.plain, target: self, action: #selector(nap))
        self.navigationItem.rightBarButtonItem = rightButton
        
        self.viewcontroller.backgroundColor = .white

               if let timeOpened = shops.TimeOpened?.doubleValue {
            let timeClosed = shops.TimeClosed?.doubleValue
            let currentTime = Double(NSDate().timeIntervalSince1970)+(7*3600)
            let currentT:Double = Double(currentTime.truncatingRemainder(dividingBy: 86400) / 3600)
            let hOdouble:Double = Double(timeOpened.truncatingRemainder(dividingBy: 86400) / 3600)
            let hCdouble:Double = Double(timeClosed!.truncatingRemainder(dividingBy: 86400) / 3600)
            let hO:Int = Int(timeOpened.truncatingRemainder(dividingBy: 86400) / 3600)
            let mO:Int = Int(timeOpened.truncatingRemainder(dividingBy: 3600) / 60)
            let hC:Int = Int(timeClosed!.truncatingRemainder(dividingBy: 86400) / 3600)
            let mC:Int = Int(timeClosed!.truncatingRemainder(dividingBy: 3600) / 60)
            var minutesOpen = "\(mO)"
            var minutesClose = "\(mC)"
            if mO < 10 {
                minutesOpen = "0\(mO)"
            }
            if mC < 10 {
                minutesClose = "0\(mC)"
            }
            hour.text = "\(hO).\(minutesOpen) - \(hC).\(minutesClose)"
//            Coupon.frame = CGRect(x:0,y:hour.frame.origin.y + hour.frame.height + 5, width: self.viewcontroller.frame.width,height:0)
//            Coupon.frame.size.height  =  self.viewcontroller.frame.height - Coupon.frame.origin.y
//            Coupon.delegate = self
//            Coupon.dataSource = self
//            
        
            
        }

        
    
    }
        func editing(){
            
            let alertController = UIAlertController(title: "", message: "Choose Action", preferredStyle: .actionSheet)
            
            let defaultAction = UIAlertAction(title: "Delete", style: .destructive, handler: self.deletetion(_:))
            let cancelaction = UIAlertAction(title:"cancel",style : .cancel,handler : nil)
            alertController.addAction(defaultAction)
            alertController.addAction(cancelaction)
            present(alertController, animated: true, completion: nil)
            
        }
    
    func deletetion(_ ss:UIAlertAction){
        let alertController = UIAlertController(title: "", message: "Choose Action", preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "Delete", style: .destructive, handler: self.deletetion2(_:))
        let cancelaction = UIAlertAction(title:"cancel",style : .default,handler : nil)
        alertController.addAction(defaultAction)
        alertController.addAction(cancelaction)
        present(alertController, animated: true, completion: nil)
       
       
    
    }
    func deletetion2(_ ss:UIAlertAction){
        let refShop = FIRDatabase.database().reference().child("shops").child(Shopname.text!)
        refShop.removeValue()
    }
    func nap(){
        
               navigationController?.popViewController(animated: true)
//        ViewController().fetchShopNcoupon()
//        ViewController().tableView.reloadData()
                dismiss(animated: true,completion:nil)
    }
    

    func estimatedsize(text  : String, sizee: CGFloat, font: String, width: CGFloat) -> CGRect{
        let CGsize  = CGSize(width: width, height: 1000)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string :  text).boundingRect(with: CGsize, options: option, attributes: [NSFontAttributeName: UIFont(name: font, size: sizee)!], context: nil)
        
    }


}

//extension Couponedit{
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return CGFloat(70)
//    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 3
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        return UITableViewCell()
//    }
//
//}
//class DetailViewCell: UICollectionViewCell {
//    
//   }


