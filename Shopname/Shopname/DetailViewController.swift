//
//  DetailViewController.swift
//  Thainy
//
//  Created by Such on 3/16/2560 BE.
//  Copyright © 2560 b.s. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseAuth
import FirebaseDatabase

class DetailViewController: UIViewController {
    
    var shop:Shop!
    
    var collectionView : UICollectionView!
    let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        NotificationCenter.default.addObserver(self, selector: #selector(DetailViewController.dds(notification:)), name: Notification.Name("fukk"), object: nil)
        
    }
    
    func dds(notification : Notification){
    editing()
    }
    func setupCollectionView() {
        collectionView = UICollectionView(frame: view.frame, collectionViewLayout: UICollectionViewFlowLayout())
        view.addSubview(collectionView)
        view.backgroundColor = .white
        collectionView.frame = view.frame
        collectionView.frame.origin.y = 20
        collectionView.frame.size.height = view.frame.height - 20
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(DetailViewCell.self, forCellWithReuseIdentifier: "xxx")
        collectionView.backgroundColor = .white
//        collectionView.showsVerticalScrollIndicator = false
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = .clear
        collectionView.refreshControl = refreshControl
        
        refreshControl.addTarget(self, action: #selector(exit), for: .valueChanged)
        
        print(shop.shop!)
    }
    
    func exit() {
        self.dismiss(animated: true, completion: nil)
    }

}

extension DetailViewController : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "xxx", for: indexPath) as! DetailViewCell
        cell.shopImageWidth = shop.widthImage as CGFloat?
        cell.shopImageHeight = shop.heightImage as CGFloat?
        
        cell.shopImage.image = nil
        let activity_image = FIRStorage.storage().reference(withPath: "shop_images/\(shop.shop!)/activity.jpg")
        if let cachedImage = imageCache.object(forKey: activity_image as AnyObject) as? UIImage {
            cell.shopImage.image = cachedImage
        } else {
            activity_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                cell.shopImage.alpha = 0
                cell.shopImage.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    cell.shopImage.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: activity_image as AnyObject)
            })
        }
        
        cell.loveImage.setImage(nil, for: .normal)
        if shop.isFavorite == true {
            cell.loveImage.setImage(UIImage(named: "like"), for: .normal)
        } else { cell.loveImage.setImage(UIImage(named: "like-black"), for: .normal) }
        cell.loveImage.addTarget(self, action: #selector(favoriteClick), for: .touchUpInside)
        
        cell.nameShop.text = shop.shop
        
        cell.ratingImage.image = nil
        if let x = shop.rateTotal {
            let rate = x as Int
            if rate < 1 {
                cell.ratingImage.image = #imageLiteral(resourceName: "dissapointment-2")
            } else if rate < 2 {
                cell.ratingImage.image = #imageLiteral(resourceName: "confused-2")
            } else if rate < 3 {
                cell.ratingImage.image = #imageLiteral(resourceName: "wink-2")
            } else {
                cell.ratingImage.image = #imageLiteral(resourceName: "flirt-2")
            }
        }
        
        cell.highlight.text = shop.descripTion
        
        cell.menu.image = nil
        let menu_image = FIRStorage.storage().reference(withPath: "shop_images/\(shop.shop!)/menu.jpg")
        if let cachedImage = imageCache.object(forKey: menu_image as AnyObject) as? UIImage {
            cell.menu.image = cachedImage
        } else {
            menu_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                cell.menu.alpha = 0
                cell.menu.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    cell.menu.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: menu_image as AnyObject)
            })
        }
        
        cell.recommendMenu1.image = nil
        let recommendMenu1_image = FIRStorage.storage().reference(withPath: "shop_images/\(shop.shop!)/recommendMenu1.jpg")
        if let cachedImage = imageCache.object(forKey: recommendMenu1_image as AnyObject) as? UIImage {
            cell.recommendMenu1.image = cachedImage
        } else {
            recommendMenu1_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                cell.recommendMenu1.alpha = 0
                cell.recommendMenu1.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    cell.recommendMenu1.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: recommendMenu1_image as AnyObject)
            })
        }
        
        cell.recommendMenu2.image = nil
        let recommendMenu2_image = FIRStorage.storage().reference(withPath: "shop_images/\(shop.shop!)/recommendMenu2.jpg")
        if let cachedImage = imageCache.object(forKey: recommendMenu2_image as AnyObject) as? UIImage {
            cell.recommendMenu2.image = cachedImage
        } else {
            recommendMenu2_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                cell.recommendMenu2.alpha = 0
                cell.recommendMenu2.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    cell.recommendMenu2.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: recommendMenu2_image as AnyObject)
            })
        }
        
        cell.recommendImageWidth1 = shop.widthImageItem1 as CGFloat?
        cell.recommendImageWidth2 = shop.widthImageItem2 as CGFloat?
        cell.recommendImageHeight1 = shop.heightImageItem1 as CGFloat?
        cell.recommendImageHeight2 = shop.heightImageItem2 as CGFloat?
        if shop.menuItem1 == nil {
            cell.isHaveRecommendMenu = false
        } else {
            cell.isHaveRecommendMenu = true
            
            cell.recommendMenu1.image = nil
            let recommend1_image = FIRStorage.storage().reference(withPath: "shop_images/\(shop.shop!)/\(shop.menuItem1!).jpg")
            if let cachedImage = imageCache.object(forKey: recommend1_image as AnyObject) as? UIImage {
                cell.recommendMenu1.image = cachedImage
            } else {
                recommend1_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                    if err != nil {
                        print(err!)
                        return
                    }
                    
                    cell.recommendMenu1.alpha = 0
                    cell.recommendMenu1.image = UIImage(data: data!)
                    UIView.animate(withDuration: 0.5, animations: {
                        cell.recommendMenu1.alpha = 1
                    }, completion: nil)
                    imageCache.setObject(UIImage(data: data!)!, forKey: recommend1_image as AnyObject)
                })
            }
            
            cell.recommendMenu2.image = nil
            let recommend2_image = FIRStorage.storage().reference(withPath: "shop_images/\(shop.shop!)/\(shop.menuItem2!).jpg")
            if let cachedImage = imageCache.object(forKey: recommend2_image as AnyObject) as? UIImage {
                cell.recommendMenu2.image = cachedImage
            } else {
                recommend2_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                    if err != nil {
                        print(err!)
                        return
                    }
                    
                    cell.recommendMenu2.alpha = 0
                    cell.recommendMenu2.image = UIImage(data: data!)
                    UIView.animate(withDuration: 0.5, animations: {
                        cell.recommendMenu2.alpha = 1
                    }, completion: nil)
                    imageCache.setObject(UIImage(data: data!)!, forKey: recommend2_image as AnyObject)
                })
            }
            
        }
        
        cell.itemMenu1.text = shop.menuItem1 //"coco"
        cell.itemMenu2.text = shop.menuItem2 //"salad"
            
        cell.priceMenu1.text = shop.priceItem1 //"50 Bath"
        cell.priceMenu2.text = shop.priceItem2 //"75 Bath"
        
        cell.imageShopFront.image = nil
        let shopFront_image = FIRStorage.storage().reference(withPath: "shop_images/\(shop.shop!)/shopFront.jpg")
        if let cachedImage = imageCache.object(forKey: shopFront_image as AnyObject) as? UIImage {
            cell.imageShopFront.image = cachedImage
        } else {
            shopFront_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                cell.imageShopFront.alpha = 0
                cell.imageShopFront.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    cell.imageShopFront.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: shopFront_image as AnyObject)
            })
        }
        
        cell.imageShopIn.image = nil
        let shopIn_image = FIRStorage.storage().reference(withPath: "shop_images/\(shop.shop!)/shopIn.jpg")
        if let cachedImage = imageCache.object(forKey: shopIn_image as AnyObject) as? UIImage {
            cell.imageShopIn.image = cachedImage
        } else {
            shopIn_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                cell.imageShopIn.alpha = 0
                cell.imageShopIn.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    cell.imageShopIn.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: shopIn_image as AnyObject)
            })
        }
        
        cell.imageShopCool.image = nil
        let shopCool_image = FIRStorage.storage().reference(withPath: "shop_images/\(shop.shop!)/shopCool.jpg")
        if let cachedImage = imageCache.object(forKey: shopCool_image as AnyObject) as? UIImage {
            cell.imageShopCool.image = cachedImage
        } else {
            shopCool_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                cell.imageShopCool.alpha = 0
                cell.imageShopCool.image = UIImage(data: data!)
                UIView.animate(withDuration: 0.5, animations: {
                    cell.imageShopCool.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: shopCool_image as AnyObject)
            })
        }
        
        if shop.phone != nil {
            cell.call.addTarget(self, action: #selector(callPhone), for: .touchUpInside)
        }
        
        cell.map.addTarget(self, action: #selector(directionMap), for: .touchUpInside)
        
        if let open = shop.TimeOpened , let close = shop.TimeClosed {
            
            let currentTime = (Float(NSDate().timeIntervalSince1970)).truncatingRemainder(dividingBy: 86400)
            
            let timeOpen = Float(open)
            let timeopenHour = timeOpen/3600
            let timeopenMinute = (timeOpen.truncatingRemainder(dividingBy: 3600))/60
            
            let timeClose = Float(close)
            let timeCloseHour = timeClose/3600
            let timeCloseMinute = (timeClose.truncatingRemainder(dividingBy: 3600))/60
            
            print(currentTime,timeOpen,timeClose)
            if currentTime < timeOpen || currentTime > timeClose {
                cell.time.text = "Close"
                cell.time.textColor = .red
            } else {
                cell.time.text = "Open \(Int(timeopenHour)):\(Int(timeopenMinute)) - \(Int(timeCloseHour)):\(Int(timeCloseMinute))"
                cell.time.textColor = .green
            }
        }
        
        cell.setupView()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let shopImageHeight = shop.heightImage! as CGFloat
        let shopImageWidth = shop.widthImage! as CGFloat
        var size = CGSize(width: view.frame.width, height: 0)
        var sizeRecommend : CGFloat?
        size.height += view.frame.width * shopImageHeight/shopImageWidth //shopImage
        size.height += view.frame.width/12 + 6// name
        size.height += estimatedsize(text: shop.descripTion!, sizee: 12, font: "Oxygen-Regular", width: view.frame.width - 7*2).height + 15 //highlight
        size.height += view.frame.width * (1920/1080) + 18 + 14 //menu
        if shop.menuItem1 != nil {
            size.height += view.frame.width/12  //youMayLike
            
            let size1 = (shop.heightImageItem1! as CGFloat)/(shop.widthImageItem1! as CGFloat)
            let size2 = (shop.heightImageItem2! as CGFloat)/(shop.widthImageItem2! as CGFloat)
            if size1 < size2 {
                sizeRecommend = size2
            } else {
                sizeRecommend = size1
            }
            size.height += (view.frame.width/2 - 4*2) * (sizeRecommend)!
            size.height += view.frame.width/12 //Price
        }
        size.height += 4 + 5 + 9
        size.height += view.frame.width * (1080/1920) + 35 // Front
        size.height += view.frame.width * (1080/1920) + 7 // In
        size.height += view.frame.width * (1080/1920) + 7 // Cool
        size.height += view.frame.width/12 + 5 //Call
        
        return size
    }
    
    func callPhone() {
        guard let number = URL(string: "telprompt://" + shop.phone!) else { return }
        UIApplication.shared.open(number, options: [:], completionHandler: nil)
    }
    
    func directionMap() {
        let destinationLat = shop.latitude! as Double
        let destinationLong = shop.longitude! as Double
        let map = URL(string: "http://maps.apple.com/?daddr=\(destinationLat),\(destinationLong)")!
        UIApplication.shared.open(map, options: [:], completionHandler: nil)
    }
    
    func favoriteClick() {
        print("Fuck man")
        let uid = FIRAuth.auth()?.currentUser?.uid
        if shop.isFavorite == false || shop.isFavorite == nil {
            FIRDatabase.database().reference().child("shops").child(shop.shop!).child("favorite").child(uid!).setValue(true)
            shop.isFavorite = true
        } else {
            FIRDatabase.database().reference().child("shops").child(shop.shop!).child("favorite").child(uid!).setValue(false)
            shop.isFavorite = false
        }
        collectionView.reloadData()
        
        //TODO: Reload CollectionView in fronter than
        
        let nc = NotificationCenter.default
        nc.post(name:Notification.Name(rawValue:"reload"),
                object: nil,
                userInfo: nil)
        
    }

    func estimatedsize(text  : String, sizee: CGFloat, font: String, width: CGFloat) -> CGRect {
        let CGsize  = CGSize(width: width, height: 1000)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string :  text).boundingRect(with: CGsize, options: option, attributes: [NSFontAttributeName: UIFont(name: font, size: sizee)!], context: nil)
        
    }
    func editing(){
        
        let alertController = UIAlertController(title: "", message: "Choose Action", preferredStyle: .actionSheet)
        
        let defaultAction = UIAlertAction(title: "Delete", style: .destructive, handler: self.deletetion(_:))
        let cancelaction = UIAlertAction(title:"cancel",style : .cancel,handler : nil)
        alertController.addAction(defaultAction)
        alertController.addAction(cancelaction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    func deletetion(_ ss:UIAlertAction){
        let alertController = UIAlertController(title: "", message: "Choose Action", preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "Delete", style: .destructive, handler: self.deletetion2(_:))
        let cancelaction = UIAlertAction(title:"cancel",style : .default,handler : nil)
        alertController.addAction(defaultAction)
        alertController.addAction(cancelaction)
        present(alertController, animated: true, completion: nil)
        
        
        
    }
    func deletetion2(_ ss:UIAlertAction){
        let refShop = FIRDatabase.database().reference().child("shops").child(shop.shop!)
        refShop.removeValue()
        let refss = FIRStorage.storage().reference().child("shop_images").child(shop.shop!)
        refss.delete(completion: { (error) -> Void in self.kuy()})
        
        
    }
    
    func kuy()  {
        self.dismiss(animated: true,completion: nil)
        print("This is deletion completo")
        
    }
}


class DetailViewCell: UICollectionViewCell {
    
    var shopImageHeight:CGFloat?
    var shopImageWidth:CGFloat?
    
    var recommendImageWidth1:CGFloat?
    var recommendImageWidth2:CGFloat?
    var recommendImageHeight1:CGFloat?
    var recommendImageHeight2:CGFloat?
    
    var isHaveRecommendMenu:Bool?
    
    let shopImage = UIImageView()
    let loveImage = UIButton()
    let nameShop = UILabel()
    let ratingImage = UIImageView()
    
    let highlight = UILabel()
    
    let menu = UIImageView()
    
    let youMayLike = UILabel()
    
    let recommendMenu1 = UIImageView()
    let itemMenu1 = UILabel()
    let priceMenu1 = UILabel()
    
    let recommendMenu2 = UIImageView()
    let itemMenu2 = UILabel()
    let priceMenu2 = UILabel()
    
    let imageShopFront = UIImageView()
    let imageShopIn = UIImageView()
    let imageShopCool = UIImageView()
    
    let call = UIButton()
    let map = UIButton()
    let time = UILabel()
    
    func setupView() {
        
        
        shopImage.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.width * shopImageHeight!/shopImageWidth!)
        addSubview(shopImage)
        let editting = UIButton()
        editting.frame = CGRect(x:self.frame.width*7/8,y:10,width: self.frame.width/14,height:self.frame.width/14)
        editting.addTarget(self, action: #selector(editingg), for: .touchUpInside)
        editting.setImage(UIImage(named:"pencil1"), for: .normal)
        loveImage.frame = CGRect(x: 7, y: shopImage.frame.maxY + 6, width: self.frame.width/14, height: self.frame.width/14)
        addSubview(loveImage)
        addSubview(editting)
        
        ratingImage.frame.size.width = loveImage.frame.width
        ratingImage.frame.size.height = loveImage.frame.height
        ratingImage.frame.origin.x = self.frame.width - ratingImage.frame.width - 7
        ratingImage.frame.origin.y = loveImage.frame.origin.y
        addSubview(ratingImage)
        
        nameShop.frame = CGRect(x: loveImage.frame.maxX + 2, y: loveImage.frame.origin.y, width: ratingImage.frame.origin.x - loveImage.frame.maxX - 2, height: ratingImage.frame.height)
        addSubview(nameShop)
        nameShop.textAlignment = .center
        nameShop.font = UIFont(name: "Oxygen-Bold", size: 15)
        nameShop.adjustsFontSizeToFitWidth = true
        
        highlight.frame = CGRect(x: loveImage.frame.origin.x, y: nameShop.frame.maxY + 15, width: self.frame.width - loveImage.frame.origin.x*2, height: nameShop.frame.height)
        addSubview(highlight)
        highlight.font = UIFont(name: "Oxygen-Regular", size: 12)
        highlight.textAlignment = .left
        highlight.numberOfLines = 10
        highlight.frame.size.height = estimatedsize(text: highlight.text!, sizee: 12, font: "Oxygen-Regular", width: highlight.frame.width).height
        
        menu.frame = CGRect(x: 0, y: highlight.frame.maxY + 18, width: self.frame.width, height: self.frame.width * (1920/1080))
        addSubview(menu)
        
        if isHaveRecommendMenu == false {
            youMayLike.frame = CGRect(x: highlight.frame.origin.x + 3, y: menu.frame.maxY + 4, width: self.frame.width - highlight.frame.origin.x, height: 0)
            
            recommendMenu1.frame = CGRect(x: highlight.frame.origin.x, y: youMayLike.frame.maxY + 5, width: self.frame.width/2 - 4*2, height: 0)
            
            recommendMenu2.frame = CGRect(x: recommendMenu1.frame.maxX + 8, y: recommendMenu1.frame.origin.y, width: self.frame.width/2 - 4*2, height: 0)
            
            itemMenu1.frame = CGRect(x: recommendMenu1.frame.origin.x + 1, y: recommendMenu1.frame.maxY + 3, width: recommendMenu1.frame.width/1.5, height: 0 )
            
            itemMenu2.frame = CGRect(x: recommendMenu2.frame.origin.x + 1, y: recommendMenu2.frame.maxY + 3, width: recommendMenu2.frame.width/1.5, height: 0)
            
            priceMenu1.frame = CGRect(x: itemMenu1.frame.maxX + 4, y: itemMenu1.frame.origin.y + 6, width: recommendMenu1.frame.width - itemMenu1.frame.width, height: 0)
            
            priceMenu2.frame = CGRect(x: itemMenu2.frame.maxX + 4, y: itemMenu2.frame.origin.y + 6, width: recommendMenu2.frame.width - itemMenu2.frame.width, height: 0)
        } else {
            
            youMayLike.frame = CGRect(x: highlight.frame.origin.x + 3, y: menu.frame.maxY + 4, width: self.frame.width - highlight.frame.origin.x, height: loveImage.frame.height)
            addSubview(youMayLike)
            youMayLike.text = "You may like"
            youMayLike.font = UIFont(name: "Oxygen-Bold", size: 15)
        
            recommendMenu1.frame = CGRect(x: highlight.frame.origin.x, y: youMayLike.frame.maxY + 5, width: self.frame.width/2 - 4*2, height: (self.frame.width/2 - 4*2) * (600/400))
            addSubview(recommendMenu1)
        
            itemMenu1.frame = CGRect(x: recommendMenu1.frame.origin.x + 1, y: recommendMenu1.frame.maxY + 3, width: recommendMenu1.frame.width/1.5, height: youMayLike.frame.height)
            addSubview(itemMenu1)
            itemMenu1.font = UIFont(name: "Oxygen-Regular", size: 14)
            let itemMenu1_size:CGSize = (itemMenu1.text! as NSString).size(attributes: [NSFontAttributeName: UIFont(name: "Oxygen-Regular", size: 14)!])
            if itemMenu1.frame.width <= itemMenu1_size.width {
                itemMenu1.adjustsFontSizeToFitWidth = true
            } else {
                itemMenu1.frame.size.width = itemMenu1_size.width
            }
        
            priceMenu1.frame = CGRect(x: itemMenu1.frame.maxX + 4, y: itemMenu1.frame.origin.y + 6, width: recommendMenu1.frame.width - itemMenu1.frame.width, height: itemMenu1.frame.height)
            addSubview(priceMenu1)
            priceMenu1.font = UIFont(name: "Oxygen-Regular", size: 14)
            priceMenu1.adjustsFontSizeToFitWidth = true
        
            recommendMenu2.frame = CGRect(x: recommendMenu1.frame.maxX + 8, y: recommendMenu1.frame.origin.y, width: self.frame.width/2 - 4*2, height: (self.frame.width/2 - 4*2) * (600/350))
            addSubview(recommendMenu2)
        
            itemMenu2.frame = CGRect(x: recommendMenu2.frame.origin.x + 1, y: recommendMenu2.frame.maxY + 3, width: recommendMenu2.frame.width/1.5, height: youMayLike.frame.height)
            addSubview(itemMenu2)
            itemMenu2.font = UIFont(name: "Oxygen-Regular", size: 14)
            let itemMenu2_size:CGSize = (itemMenu2.text! as NSString).size(attributes: [NSFontAttributeName: UIFont(name: "Oxygen-Regular", size: 14)!])
            if itemMenu2.frame.width <= itemMenu2_size.width {
                itemMenu2.adjustsFontSizeToFitWidth = true
            } else {
                itemMenu2.frame.size.width = itemMenu2_size.width
            }
        
            priceMenu2.frame = CGRect(x: itemMenu2.frame.maxX + 4, y: itemMenu2.frame.origin.y + 6, width: recommendMenu2.frame.width - itemMenu2.frame.width, height: itemMenu2.frame.height)
            addSubview(priceMenu2)
            priceMenu2.font = UIFont(name: "Oxygen-Regular", size: 14)
            priceMenu2.adjustsFontSizeToFitWidth = true
        }
        
        imageShopFront.frame = CGRect(x: 0, y: itemMenu2.frame.maxY + 35, width: self.frame.width, height: self.frame.width * (1080/1920))
        addSubview(imageShopFront)
        
        if recommendMenu1.frame.height > recommendMenu2.frame.height {
            imageShopFront.frame.origin.y = itemMenu1.frame.maxY + 35
        }

        imageShopIn.frame = CGRect(x: 0, y: imageShopFront.frame.maxY + 7, width: self.frame.width, height: self.frame.width * (1080/1920))
        addSubview(imageShopIn)
        
        imageShopCool.frame = CGRect(x: 0, y: imageShopIn.frame.maxY + 7, width: self.frame.width, height: self.frame.width * (1080/1920))
        addSubview(imageShopCool)
        
        call.frame = CGRect(x: 4, y: imageShopCool.frame.maxY + 5, width: loveImage.frame.width, height: loveImage.frame.width)
        addSubview(call)
        call.setImage(#imageLiteral(resourceName: "phone"), for: .normal)
        
        map.frame = CGRect(x: call.frame.maxX + 15, y: call.frame.origin.y, width: call.frame.width, height: call.frame.height)
        addSubview(map)
        map.setImage(#imageLiteral(resourceName: "location"), for: .normal)
        
        time.frame = CGRect(x: map.frame.maxX + 3, y:map.frame.origin.y, width: self.frame.width - (map.frame.maxX + 10), height:map.frame.height)
        addSubview(time)
        time.font = UIFont(name: "Oxygen-Bold", size: 15)
        time.textAlignment = .right
        
        
        
        
        shopImage.backgroundColor = .gray
        menu.backgroundColor = .gray
        recommendMenu1.backgroundColor = .gray
        recommendMenu2.backgroundColor = .gray
        imageShopCool.backgroundColor = .gray
        imageShopIn.backgroundColor = .gray
        imageShopFront.backgroundColor = .gray
        
    }

    
    func estimatedsize(text  : String, sizee: CGFloat, font: String, width: CGFloat) -> CGRect {
        let CGsize  = CGSize(width: width, height: 1000)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string :  text).boundingRect(with: CGsize, options: option, attributes: [NSFontAttributeName: UIFont(name: font, size: sizee)!], context: nil)
        
    }
    
    func editingg() {
    
    NotificationCenter.default.post(name: Notification.Name("fukk"), object: nil)
    }
    override init(frame: CGRect){
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
