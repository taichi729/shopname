//
//  LoginController.swift
//  AppMain
//
//  Created by Such on 12/23/2559 BE.
//  Copyright © 2559 b.s. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
import FBSDKLoginKit
import SwiftyJSON

class LoginController: UIViewController, FBSDKLoginButtonDelegate {
    
    var imageBackground : UIImageView!
    let screen = UIScreen.main.bounds
    
    let ProfileImage : UIImageView = {
        let profile = UIImageView()
        return profile
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNofitication()
        setupImageBackground()
        let loginButton = FBSDKLoginButton()
        view.addSubview(loginButton)
        loginButton.frame = CGRect(x: 16, y: view.bounds.maxY - 100, width: view.frame.width - 40, height: 70)
        loginButton.delegate = self
        loginButton.readPermissions = ["email","public_profile","user_friends"]
        
        
    }
    
    func setupImageBackground(){
        let frame = CGRect(x: 0, y: 0, width: screen.width, height: screen.height)
        imageBackground = UIImageView(frame: frame)
        imageBackground.image = UIImage(named: "backgroundLogin")
        view.addSubview(imageBackground)
    }
    
    func setupNofitication(){
        NotificationCenter.default.addObserver(self, selector: #selector(saveImageToFirebase), name: NSNotification.Name(rawValue: "Firebase-imageLoaded"), object: nil)
    }
    
    func saveImageToFirebase(){
        let imageName = FIRAuth.auth()!.currentUser!.displayName!
        let storageRef = FIRStorage.storage().reference().child("profile_images").child("\(imageName).png")
        if let uploadData = UIImagePNGRepresentation(self.ProfileImage.image!){
            storageRef.put(uploadData, metadata: nil, completion: { (metadata, err) in
                if err != nil {
                    print(err!)
                    return
                }
                print("metadata",metadata!)
                self.getIdfacebook()
                self.getIdfriendsfacebook()
                self.dismiss(animated: true, completion: nil)
            })
        }
        
    }
    
    func getIdfacebook(){
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields":"id"]).start { (connection, result, err) in
            if err != nil {
                print("error to get id facebook",err!)
                return
            }
            let data:[String:AnyObject] = result as! [String : AnyObject]
            let id = data["id"] as! String
            let uid = FIRAuth.auth()!.currentUser!.uid
            let ref = FIRDatabase.database().reference(fromURL: "https://phuket-treasure.firebaseio.com/")
            let userRef = ref.child("users").child(uid)
            let values = ["id_facebook":id]
            userRef.updateChildValues(values)
        }
    }
    
    func getIdfriendsfacebook(){
        FBSDKGraphRequest(graphPath: "/me/friends", parameters: ["fields":"id"]).start{ (connection, result, err) in
            if err != nil {
                print("Failed to start graph request:", err!)
                return
            }
            var json = JSON(result!)
            let data = json["data"].arrayValue
            let uid = FIRAuth.auth()?.currentUser?.uid
            let ref = FIRDatabase.database().reference(fromURL: "https://phuket-treasure.firebaseio.com/")
            let friendRef = ref.child("users").child(uid!).child("friends")
            for friend in data {
                let id = friend["id"].stringValue
                friendRef.child(id).setValue(true)
                
            }
        }
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil{
            print(error)
            return
        }
        loginWithFirebase()
        
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Did log out")
    }
    
    func loginWithFirebase(){
        let accessToken = FBSDKAccessToken.current()
        guard let accessTokenString = accessToken?.tokenString else { return }
        let credentails = FIRFacebookAuthProvider.credential(withAccessToken: accessTokenString)
        FIRAuth.auth()?.signIn(with: credentails, completion: { (user, err) in
            if err != nil {
                print("error sign in to Firebase",err!)
                return
            }
            print("success login to Firebase",user!)
            guard let uid = user?.uid else {return}
            guard let profileUrl = user?.photoURL else {return}
         
            
            let ref = FIRDatabase.database().reference(fromURL: "https://phuket-treasure.firebaseio.com/")
            let userRef = ref.child("users").child(uid)
            
            let name = user?.displayName as! NSString
            let email = user?.email as! NSString
            let values = ["name":name,"email":email]
            
            userRef.updateChildValues(values)
            self.dismiss(animated: true, completion: nil)
            
        })
    }
    
}
