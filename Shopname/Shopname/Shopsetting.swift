//
//  Shopsetting.swift
//  Shopname
//
//  Created by MAC_OS on 2/18/2560 BE.
//  Copyright © 2560 MAC_OS. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase
import FBSDKCoreKit
import FirebaseAuth
import FirebaseStorage
import DKImagePickerController
class Shopsetting : UIViewController,UIPickerViewDelegate,UITabBarDelegate,UITextViewDelegate{
    var Shopname =  UILabel()
    var selecterphotoforsave = UIImage()
    var shops = [Shop()]
    var imagesforother : [DKAsset]?
    var imagesforiteminmenu :  [DKAsset]?
    var imagesformenu: [DKAsset]?
    var imagesforotherimage = [UIImage]()
    var imagesforiteminmenuimage =  [UIImage]()
    var imagesformenuimage = [UIImage]()
    var imagesforotherview = [UIImageView]()
    var imagesforiteminmenuview =  [UIImageView]()
    var imagesformenuview = UIImageView()
    let Pic = UIButton()
    let Serviceicon = UIButton()
    let Restauranticon = UIButton()
    let Travelicon = UIButton()
    let Attractionicon = UIButton()
    let Othersicon = UIButton()
    let shopnamefield = UITextView()
    let descriptionfield = UITextView()
    let hour = UILabel()
    let openpicker = PDatePicker()
    let closepicker = UIDatePicker()
    let Tag = UILabel()
    let Tagfield = UITextView()
    let imageforotherbutton = UILabel()
    let imageformenubutton = UILabel()
    let imageforiteminmenubutton = UILabel()
    let address = UILabel()
    let addreesstextfield = UITextView()
    let web = UILabel()
    let webfield = UITextView()
    let linefield = UITextView()
    let facebookfield = UITextView()
    let emailfield = UITextView()
    let phonefield = UITextView()
    let save = UIButton()
    let opentext = "00:00"
    let closetext = "24:00"
    let timeformatter1 = DateFormatter()
    let timeformatter2 = DateFormatter()
    let timeforsave1 = DateFormatter()
    let timeforsave2 = DateFormatter()
    let opentime = UILabel()
    let between = UILabel()
    let closetime = UILabel()
    let viewcontroller = UIScrollView()
    var timeop = 0
    var timecl = (24*60*60)
    var icontype = NSString()
    var item1 = [UITextView]()
    
var startcontentsize = CGFloat()
    func handleLogout() {
        do {
            try FIRAuth.auth()?.signOut()
        } catch let err {
            print(err)
        }
        let vc = LoginController()
        self.present(vc, animated: true, completion: nil)
    }
    func isUserLogedin() {
        if FIRAuth.auth()?.currentUser == nil {
            handleLogout()
            return
        }
    }
     override func viewDidLoad() {
        super.viewDidLoad()
       
         UILabel.appearance().font = UIFont(name:"Oxygen-Light",size: 38)
        isUserLogedin()
        
        let navigationItem = UINavigationItem()
        navigationItem.title = "Profile"
        let rightButton = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(Save))
        
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title:"<Back",style:   UIBarButtonItemStyle.plain, target: self, action: #selector(nap))
        self.navigationItem.rightBarButtonItem = rightButton

        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.alpha = 1
         shopnamefield.delegate = self
         descriptionfield.delegate = self
        Tagfield.delegate = self
    
         addreesstextfield.delegate = self
                 webfield.delegate = self
         linefield.delegate = self
         facebookfield.delegate = self
         emailfield.delegate = self
         phonefield.delegate = self
        openpicker.datePickerMode = .time
        viewcontroller.alwaysBounceVertical = true
        viewcontroller.bouncesZoom = false
        viewcontroller.showsHorizontalScrollIndicator = false
        viewcontroller.scrollsToTop = true
        

        
       
        
        Shopname.text = "Phuket Treasure"
        
        viewcontroller.frame = CGRect(x:0,y:0,width:self.view.frame.width,height:self.view.frame.height
        )
        viewcontroller.contentSize = CGSize(width:self.view.frame.width,height: 10000)
        Shopname.frame = CGRect(x:6,y:10,width : self.view.frame.width,height: 40)
        Shopname.font = UIFont.systemFont(ofSize: 38)
        Shopname.font = UIFont(name:"Oxygen-Light",size: 38)
        Shopname.numberOfLines = 1
        Shopname.adjustsFontSizeToFitWidth = true
        view.addSubview(viewcontroller)
        viewcontroller.addSubview(Shopname)
    
        
        Pic.frame = CGRect(x:self.view.frame.width/12,y: 16 + 34 + 6, width: self.view.frame.width*5/6,height:self.view.frame.width/2)
        Pic.imageView?.contentMode = .scaleAspectFill
        Pic.setImage(UIImage(named:"images.png"), for : .normal)
        let Piclayer = UIView()
        let Picupload = UILabel()
        let Picress = UIView()
        Piclayer.frame.size = CGSize(width:Pic.frame.width,height:Pic.frame.height/4)
        Piclayer.frame.origin.y = Pic.frame.height*3/4
        Picress.frame = Piclayer.frame
        Piclayer.backgroundColor = .black
        Piclayer.alpha = 0.5
        Piclayer.isUserInteractionEnabled = true
//        let gesssss = UITapGestureRecognizer(target:self ,action : #selector(self.pickerimage(_:)))
        Pic.addTarget(self, action: #selector(self.pickerimage), for: .touchUpInside)
        Picress.isUserInteractionEnabled = true
        Picupload.text = "Upload image"
        Picupload.frame.size = CGSize(width:Pic.frame.width,height:Piclayer.frame.height*2/3)
        Picupload.adjustsFontSizeToFitWidth = true
        Picupload.textAlignment = .center
        Picupload.center = Piclayer.center
    Pic.contentMode = .scaleAspectFit
        Pic.addSubview(Picupload)
        Pic.addSubview(Piclayer)
        Pic.addSubview(Picress)
        Tagfield.roundcorner()
        addreesstextfield.roundcorner()
        webfield.roundcorner()
        linefield.roundcorner()
        facebookfield.roundcorner()
        phonefield.roundcorner()
        emailfield.roundcorner()
        
        Serviceicon.frame.size = CGSize(width:self.view.frame.width/6,height:self.view.frame.width/6)
        Restauranticon.frame.size = Serviceicon.frame.size
        Travelicon.frame.size = Serviceicon.frame.size
        Travelicon.setImage(UIImage(named:"travel"), for: .normal)
        Travelicon.roundcorner()
        Attractionicon.frame.size = Serviceicon.frame.size
        Othersicon.frame.size = Serviceicon.frame.size
        
        Serviceicon.frame.origin.x = self.view.frame.width/(6*6) + Serviceicon.frame.width/2
        Serviceicon.frame.origin.y = Pic.frame.height + Pic.frame.origin.y + 10
        Serviceicon.backgroundColor = .lightGray
        Serviceicon.setImage(UIImage(named:"service"), for: .normal)
        Restauranticon.frame.origin.x = self.view.frame.width/(6*6) + Serviceicon.frame.origin.x + self.view.frame.width/6
        Restauranticon.frame.origin.y =  Pic.frame.height + Pic.frame.origin.y + 10
        Restauranticon.backgroundColor = .lightGray
        Restauranticon.setImage(UIImage(named:"restaurant"), for: .normal)
        Travelicon.frame.origin.x = self.view.frame.width/(6*6) + Restauranticon.frame.origin.x + self.view.frame.width/6
        
        Travelicon.frame.origin.y = Pic.frame.height + Pic.frame.origin.y + 10
        Travelicon.backgroundColor = .lightGray
        Attractionicon.frame.origin.x = self.view.frame.width/(6*6) + Travelicon.frame.origin.x + self.view.frame.width/6
        
        Attractionicon.frame.origin.y =  Pic.frame.height + Pic.frame.origin.y + 10
        Attractionicon.backgroundColor = .lightGray
        Attractionicon.setImage(UIImage(named:"swim"), for: .normal)
        Othersicon.frame.origin.x = self.view.frame.width/(6*6) + Attractionicon.frame.origin.x + self.view.frame.width/6
        
        Othersicon.frame.origin.y = Pic.frame.height + Pic.frame.origin.y + 10
        Othersicon.backgroundColor = UIColor.lightGray
        Othersicon.setImage(UIImage(named:"others"), for: .normal)
        Attractionicon.roundcorner()
        Othersicon.roundcorner()
        Restauranticon.roundcorner()
        Serviceicon.roundcorner()
        shopnamefield.frame = CGRect(x:Pic.frame.origin.x,y:Serviceicon.frame.origin.y + Serviceicon.frame.height + 10,width: Pic.frame.width,height:32)
        shopnamefield.text = "Shop Name"
        shopnamefield.roundcorner()
        //        shopnamefield.
        
        descriptionfield.frame = CGRect(x:shopnamefield.frame.origin.x,y:shopnamefield.frame.origin.y + shopnamefield.frame.height + 10,width:Pic.frame.width,height:100)
        descriptionfield.delegate = self
        descriptionfield.frame.size.height = 50
        descriptionfield.text = "Description"
        descriptionfield.roundcorner()
        
        
        hour.frame = CGRect(x: shopnamefield.frame.origin.x,y: descriptionfield.frame.origin.y + descriptionfield.frame.height + 5,width: 100, height: 50)
        hour.text = "hour: "
        hour.font = UIFont(name:"Oxygen-Light",size: 28)
        hour.frame.size.width = hour.intrinsicContentSize.width
        hour.numberOfLines = 1
        
        
        opentime.frame = hour.frame
        opentime.text = opentext
        opentime.font = hour.font
        opentime.frame.size.width = opentime.intrinsicContentSize.width + 1
        opentime.adjustsFontSizeToFitWidth = true
        between.frame = hour.frame
        between.text = " ~ "
        between.font = hour.font
        between.frame.size.width = between.intrinsicContentSize.width + 1
        closetime.frame = hour.frame
        closetime.text = closetext
        closetime.font = hour.font
        closetime.adjustsFontSizeToFitWidth = true
        
        closetime.frame.size.width = closetime.intrinsicContentSize.width + 1
        opentime.frame.origin.x  = hour.frame.origin.x + hour.frame.width
        between.frame.origin.x = opentime.frame.origin.x + opentime.frame.width
        
        closetime.frame.origin.x = opentime.frame.origin.x + opentime.frame.width + between.frame.width
        let gesture = Mygesture(target:self,action: #selector(self.startpicker(_:)))
        gesture.int  = 1
        opentime.isUserInteractionEnabled = true
        opentime.addGestureRecognizer(gesture)
        
        
        closetime.isUserInteractionEnabled = true
        let gesture2 = Mygesture(target:self,action: #selector(self.startpicker(_:)))
        gesture2.int = 2
        closetime.addGestureRecognizer(gesture2)
        let ssd = self.view.frame
        openpicker.frame = CGRect(x:0,y:ssd.height*2/3,width: ssd.width,height: ssd.height/3)
        
       
        openpicker.backgroundColor = .white
        openpicker.locale = NSLocale(localeIdentifier: "da_DK") as Locale
        openpicker.minuteInterval = 5
        
        timeformatter1.timeStyle = DateFormatter.Style.short
        timeformatter1.dateFormat = "HH:mm"
        timeformatter2.timeStyle = DateFormatter.Style.short
        timeformatter2.dateFormat = "HH:mm"
        timeforsave1.timeStyle = DateFormatter.Style.short
        timeforsave1.dateFormat = "mm"
        timeforsave2.timeStyle = DateFormatter.Style.short
        timeforsave2.dateFormat = "HH"
        Tag.frame = CGRect(x:Pic.frame.origin.x,y:opentime.frame.origin.y+opentime.frame.height+10,width:Pic.frame.width,height : 30)
        Tag.text = "Tag"
        Tagfield.frame = CGRect(x:Pic.frame.origin.x,y:Tag.frame.origin.y+Tag.frame.height+5,width:Pic.frame.width,height:34)
        Tagfield.text = "shop,phuket,..."
        //
        //
        //
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        //
        //
        
        imageforotherbutton.frame = hour.frame
        imageforotherbutton.frame.origin.x = hour.frame.origin.x-hour.frame.origin.x/2
        imageforotherbutton.frame.origin.y = hour.frame.maxY  + 7
        imageforotherbutton.frame.size.width = self.view.frame.width*2/3
        imageforotherbutton.frame.size.height = imageforotherbutton.frame.height*4/5
        imageforotherbutton.text = " Upload Galleries "
        imageforotherbutton.font =  hour.font
        imageforotherbutton.backgroundColor = .white
        imageforotherbutton.roundcorner()
        imagesforotherview = []
                let gother = gees(target:self,action: #selector(self.picother))
        imageforotherbutton.isUserInteractionEnabled = true
        imageforotherbutton.addGestureRecognizer(gother)
        for ww in 0...2 {
            let iv = UIImageView()
            if ww == 0 {
                
                iv.frame.origin = CGPoint(x:0,y:imageforotherbutton.frame.maxY + 7)
                
            }else{
        
            iv.frame.origin = CGPoint(x:(imagesforotherview[ww-1].frame.origin.x),y:imagesforotherview[ww-1].frame.maxY+5)
            }
            iv.backgroundColor = .blue
        imagesforotherview.append(iv)
            
        }
        print(imagesforotherview.count,"the worst")
        //
        //
        //
        //
        //
        
        imageformenubutton.frame = imageforotherbutton.frame
        imageformenubutton.frame.origin.y = (imagesforotherview[2].frame.maxY)+7
        imageformenubutton.font = imageforotherbutton.font
        imageformenubutton.font = imageformenubutton.font.withSize(20)
                imageformenubutton.text = " Upload Menu or Brochure image "
        imageforotherbutton.font = imageforotherbutton.font.withSize(20)
        imageformenubutton.frame.size.width = imageformenubutton.intrinsicContentSize.width + 2
imageformenubutton.backgroundColor = .white
        imageforotherbutton.frame.size.width = imageforotherbutton.intrinsicContentSize.width + 2
        imagesformenuview.frame.origin = CGPoint(x:0,y:imageformenubutton.frame.maxY + 7)
        imagesformenuview.backgroundColor = .blue
        let gmenu = gees(target:self,action: #selector(self.picmenu))
        imageformenubutton.isUserInteractionEnabled = true
        imageformenubutton.addGestureRecognizer(gmenu)
        imageformenubutton.roundcorner()
        //
        //
        //
        //
        //
        imageforiteminmenubutton.frame = imageforotherbutton.frame
        imageforiteminmenubutton.backgroundColor = .white
        imageforiteminmenubutton.frame.origin.y = ((imagesformenuview.frame.origin.y)+(imagesformenuview.frame.height)) + 7
        imageforiteminmenubutton.font = imageforotherbutton.font.withSize(20)
         imageforiteminmenubutton.text = " Upload item photo "
        imageforiteminmenubutton.frame.size.width = imageforiteminmenubutton.intrinsicContentSize.width + 2
        
       
        imageforiteminmenubutton.font.withSize(imageformenubutton.font.pointSize)
        imageforiteminmenubutton.roundcorner()
        imagesforiteminmenuview = []
        let gitem = gees(target:self,action: #selector(self.picitem))
        imageforiteminmenubutton.isUserInteractionEnabled = true
        imageforiteminmenubutton.addGestureRecognizer(gitem)
            let iv1 = UIImageView()
            let iv2 = UIImageView()
                
                iv1.frame.origin = CGPoint(x:0,y:imageforiteminmenubutton.frame.maxY + 7)
                
        imagesforiteminmenuview.append(iv1)
                iv2.frame.origin = CGPoint(x:(imagesforiteminmenuview[0].frame.origin.x),y:(imagesforiteminmenuview[0].frame.origin.y)+(imagesforiteminmenuview[0].frame.height)+5)
        imagesforiteminmenuview.append(iv2)
        for _ in 0...1{
            let tt = UITextView()
            tt.roundcorner()
            tt.center.x = self.view.center.x
            item1.append(tt)
            
        }
        
        
        
        web.frame  = Tag.frame
        web.frame.origin = CGPoint(x:Pic.frame.origin.x,y:(imagesforiteminmenuview[1].frame.origin.y)+(imagesforiteminmenuview[1].frame.height)+7)
//        web.frame.origin.y = Tagfield.frame.origin.y + Tagfield.frame.height + 10
        web.text = "Contact"
        web.font = UIFont(name:"Oxygen-Light",size: 32)
        addreesstextfield.frame = Tag.frame
        addreesstextfield.frame.origin.y = web.frame.origin.y  + web.frame.height + 10
        addreesstextfield.text = "address"
        phonefield.frame = Tag.frame
        phonefield.frame.origin.y = addreesstextfield.frame.origin.y  + addreesstextfield.frame.height + 10
        phonefield.text = "phone"
        webfield.frame = Tag.frame
        webfield.frame.origin.y = phonefield.frame.origin.y  + phonefield.frame.height + 10
        webfield.text = "website"
        linefield.frame = Tag.frame
        linefield.frame.origin.y = webfield.frame.origin.y  + webfield.frame.height + 10
        linefield.text = "line"
        facebookfield.frame = Tag.frame
        facebookfield.frame.origin.y = linefield.frame.origin.y  + linefield.frame.height + 10
        facebookfield.text = "facebook"
        emailfield.frame = Tag.frame
        emailfield.frame.origin.y = facebookfield.frame.origin.y  + facebookfield.frame.height + 10
        emailfield.text = "email"
        viewcontroller.contentSize.height = emailfield.frame.origin.y + emailfield.frame.height + 30
        
       
        
        
        
        viewcontroller.backgroundColor = UIColor(red: 150/255,green : 200/255,blue: 250/255,alpha:1
        )
        viewcontroller.addSubview(Serviceicon)
        viewcontroller.addSubview(Restauranticon)
        viewcontroller.addSubview(Travelicon)
        viewcontroller.addSubview(Attractionicon)
//        viewcontroller.addSubview(Othersicon)
        viewcontroller.addSubview(shopnamefield)
        viewcontroller.addSubview(descriptionfield)
        viewcontroller.addSubview(hour)
        viewcontroller.addSubview(opentime)
        viewcontroller.addSubview(closetime)
        viewcontroller.addSubview(between)
//        viewcontroller.addSubview(Tag)
//        viewcontroller.addSubview(Tagfield)
   viewcontroller.addSubview(imageforotherbutton)
           viewcontroller.addSubview(imageforiteminmenubutton)
           viewcontroller.addSubview(imageformenubutton)
        for www in imagesforiteminmenuview {
        viewcontroller.addSubview(www)
        }
        for www in imagesforotherview {
        viewcontroller.addSubview(www)
        
        }
        viewcontroller.addSubview(imagesformenuview)
        viewcontroller.addSubview(Attractionicon)
        
                viewcontroller.addSubview(web)
                viewcontroller.addSubview(webfield)
                viewcontroller.addSubview(emailfield)
                viewcontroller.addSubview(facebookfield)
                viewcontroller.addSubview(linefield)
                viewcontroller.addSubview(phonefield)
                viewcontroller.addSubview(addreesstextfield)
                viewcontroller.addSubview(Pic)
        Serviceicon.addTarget(self, action: #selector(changecolor(_:)), for: .touchUpInside)
        Restauranticon.addTarget(self, action: #selector(changecolor(_:)), for: .touchUpInside)
        Travelicon.addTarget(self, action: #selector(changecolor(_:)), for: .touchUpInside)
        Attractionicon.addTarget(self, action: #selector(changecolor(_:)), for: .touchUpInside)
        Othersicon.addTarget(self, action: #selector(changecolor(_:)), for: .touchUpInside)

     startcontentsize  = self.viewcontroller.contentSize.height
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
      switch textView
      {
      case shopnamefield : if textView.text == "Shop Name" {
     textView.text = ""
      
      };break
      case descriptionfield : if textView.text == "Description" {
        textView.text = ""
        
      };break
      case addreesstextfield : if textView.text == "address" {
        textView.text = ""
        
      };break
      case phonefield : if textView.text == "phone" {
        textView.text = ""
        
      };break
      case facebookfield : if textView.text == "facebook" {
        textView.text = ""
        
      };break
      case linefield : if textView.text == "line" {
        textView.text = ""
        
      };break
      case webfield : if textView.text == "website" {
        textView.text = ""
        
      };break
      case emailfield : if textView.text == "email" {
        textView.text = ""
        
      };break
      case item1[0] : if textView.text == "item name" {
        textView.text = ""
        
      };break
      case item1[1] : if textView.text == "item name"{
        textView.text = ""
        
      };break

      
      default:break
        }
//        textView.text = ""
    }
    func colorgray(){
        Serviceicon.backgroundColor = .lightGray

        Attractionicon.backgroundColor = .lightGray

        Othersicon.backgroundColor = .lightGray

        Travelicon.backgroundColor = .lightGray

        Restauranticon.backgroundColor = .lightGray

    
    }
    func changecolor(_ target :  UIButton) {
        print("fuckyou")
        colorgray()
        switch target
        {
        case Serviceicon :
            UIView.animate(withDuration: 0.5, animations: {
                
            
            
            self.viewcontroller.backgroundColor = UIColor(red: 0.63,green:0.87,blue: 1,alpha :1)
            })
            icontype = "service"
            Serviceicon.backgroundColor = UIColor(red: 0.2,green:0.87,blue: 1,alpha :1)
             break
        case Restauranticon :
            UIView.animate(withDuration: 0.5, animations: {
            self.viewcontroller.backgroundColor = UIColor(red: 1,green:0.66,blue: 0.4,alpha :1)
            })
            icontype = "food"
            Restauranticon.backgroundColor = UIColor(red: 1,green:0.4,blue: 0.2,alpha :1)
            break
        case Travelicon :
                UIView.animate(withDuration: 0.5, animations: {
            self.viewcontroller.backgroundColor = UIColor(red: 0.1,green:0.8,blue: 0.1,alpha :1)
            })
                icontype = "travel"
                Travelicon.backgroundColor = UIColor(red: 0.4,green:0.81,blue: 0.3,alpha :1)
                    break
        case Attractionicon :
                UIView.animate(withDuration: 0.5, animations: {
            self.viewcontroller.backgroundColor = UIColor(red: 0.93,green:0.98,blue: 0.32,alpha :1)
             })
                icontype = "attraction"
                Attractionicon.backgroundColor = UIColor(red: 0.8,green:0.8,blue: 0.1,alpha :1)
                    break
        case Othersicon :
            UIView.animate(withDuration: 0.5, animations: {
            self.viewcontroller.backgroundColor = UIColor(red: 0.63,green:0.42,blue: 0.88,alpha :1)
            })
            icontype = "others"
            Othersicon.backgroundColor = UIColor(red: 0.53,green:0.32,blue: 0.88,alpha :1)
                break
        default : break
        }
    
    }
    func positioning() {
        
        shopnamefield.frame.origin = CGPoint(x:Pic.frame.origin.x,y:Serviceicon.frame.origin.y + Serviceicon.frame.height + 10)

        descriptionfield.frame.origin = CGPoint(x:shopnamefield.frame.origin.x,y:shopnamefield.frame.origin.y + shopnamefield.frame.height + 10)
 
        
        hour.frame.origin = CGPoint(x: shopnamefield.frame.origin.x,y: descriptionfield.frame.origin.y + descriptionfield.frame.height + 5)
        
        
        opentime.frame.origin.y = hour.frame.origin.y
   opentime.backgroundColor = .white
        closetime.backgroundColor = .white
        opentime.roundcorner()
        closetime.roundcorner()
        between.frame.origin.y = hour.frame.origin.y

        closetime.frame.origin.y = hour.frame.origin.y
              imageforotherbutton.frame.origin.y = hour.frame.maxY  + 7
        for ww in 0...2 {
           
            if ww == 0 {
                
                imagesforotherview[ww].frame.origin = CGPoint(x:0,y:imageforotherbutton.frame.maxY + 7)
                
            }else{
                
                imagesforotherview[ww].frame.origin = CGPoint(x:(imagesforotherview[ww-1].frame.origin.x),y:(imagesforotherview[ww-1].frame.origin.y)+(imagesforotherview[ww-1].frame.height)+5)
            }
        }
   
imageformenubutton.frame.origin.y = (imagesforotherview[2].frame.maxY)+7
        imagesformenuview.frame.origin = CGPoint(x:0,y:imageformenubutton.frame.maxY + 7)
           imageforiteminmenubutton.frame.origin.y = ((imagesformenuview.frame.origin.y)+(imagesformenuview.frame.height)) + 7
        for ww in 0...1 {
            let iv = UIImageView()
            if ww == 0 {
                
                imagesforiteminmenuview[0].frame.origin.y = (imageforiteminmenubutton.frame.maxY + 7)
                item1[0].frame.origin.y = (imagesforiteminmenuview[0].frame.maxY + 5)

            }else{
                
                imagesforiteminmenuview[1].frame.origin.y = ((item1[0].frame.maxY)+5)
                item1[1].frame.origin.y = (imagesforiteminmenuview[1].frame.maxY + 5)
            }
            
        }
        print(imagesforiteminmenuview[1].frame.origin.y)
        print(imagesforiteminmenuview[1].frame.maxY)
        print(imagesforiteminmenuview[1].frame.height)
        web.frame.origin = CGPoint(x:Pic.frame.origin.x,y:item1[1].frame.maxY+7)
      
        addreesstextfield.frame.origin.y = web.frame.origin.y  + web.frame.height + 10
              phonefield.frame.origin.y = addreesstextfield.frame.origin.y  + addreesstextfield.frame.height + 10
      
        webfield.frame.origin.y = phonefield.frame.origin.y  + phonefield.frame.height + 10
                linefield.frame.origin.y = webfield.frame.origin.y  + webfield.frame.height + 10
        
        facebookfield.frame.origin.y = linefield.frame.origin.y  + linefield.frame.height + 10
                emailfield.frame.origin.y = facebookfield.frame.origin.y  + facebookfield.frame.height + 10
        self.viewcontroller.contentSize.height = emailfield.frame.origin.y + emailfield.frame.height + 20
        
    
    }
    
    let space = UIView()
    func startpicker(_ uidate: Mygesture){
        print("dddddfsa")
        space.frame = view.frame
        space.isUserInteractionEnabled = true
        let jesture = UITapGestureRecognizer(target:self, action: #selector(pickerdestroyer(_:)))
        space.addGestureRecognizer(jesture)
        switch uidate.int{
        case 1:
            openpicker.alpha = 1
            openpicker.int  = 1
            let timformatforwhile = timeformatter1.date(from: opentime.text!)
            openpicker.setDate(timformatforwhile!, animated: false)
            self.view.addSubview(space)
            self.view.addSubview(openpicker)
            break
        case 2:
            var closed = closetime.text
            if closetime.text == "24:00"
            {
            closed = "00:00"
            }
            let timformatforwhile2 = timeformatter1.date(from: closed!)
//            timecl = timeforsave2.date(from:closed!)
            openpicker.setDate(timformatforwhile2!, animated: false)
            openpicker.alpha = 1
            openpicker.int = 2
        self.view.addSubview(space)
        self.view.addSubview(openpicker)
            break
            
        default:
            //        while true {}
            print("run picker")
            break
        }
    }
    let descripinit = CGFloat(50)
    let tagint = CGFloat(34)
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let s = UITextView()
        s.frame = textView.frame
        s.text = textView.text
        print(s.text)
        var datbitch =  adjusttext(textView)
        
        //
        if textView == descriptionfield{
            
            if descripinit >= datbitch{
                textView.frame.size.height = CGFloat(50)
                
            }else if datbitch > descripinit{
                descriptionfield.frame.size.height = datbitch
            }
            
            if descriptionfield.text == "description"{
            return true
            }
        }
        
        if textView == Tagfield {
            if tagint >= datbitch {
                textView.frame.size.height = tagint
                
            } else if datbitch > tagint{
                Tagfield.frame.size.height = datbitch
            }
        
        }
        positioning()

    
    return true
    }
    
    func adjusttext(_ obj: UITextView)-> CGFloat{
        
        return obj.contentSize.height;
        
        
    }
    let caution = UIImageView()
    var descriptions = UILabel()
    let goback = UIButton()
    let cancel = UIButton()
    let makesure = UIView()
    func nap(){
        
        let alertController = UIAlertController(title: "All unsave data will be lost", message: "", preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "Back", style: .default, handler: self.back(_:))
        let cancelaction = UIAlertAction(title:"Cancel",style : .default,handler : nil)
        alertController.addAction(defaultAction)
        alertController.addAction(cancelaction)
        present(alertController, animated: true, completion: nil)
////        makesure.backgroundColor = UIColor.black.withAlphaComponent(0.65)
//        makesure.backgroundColor = .white
//        makesure.frame = self.view.frame
//       
//        caution.backgroundColor = .white
//        caution.frame = CGRect(x:0,y:Pic.frame.origin.y+Pic.frame.height/4,width: 150,height:150)
//        caution.center.x = self.view.frame.width/2
//        descriptions.frame = CGRect(x:0,y:caution.frame.origin.y+caution.frame.height + 15,width:Pic.frame.width,height:800)
//        descriptions.text = "Do you really want to go back"
//        descriptions.center.x = caution.center.x
//        descriptions.font = UIFont.systemFont(ofSize: 28)
//        descriptions.numberOfLines = 2
//        descriptions.backgroundColor = .green
//        descriptions.frame.size.height=estimatedsize(text:descriptions.text!, sizee: 28).height+2
//        goback.frame.origin = CGPoint(x:Pic.frame.origin.x,y:descriptions.frame.origin.y+descriptions.frame.height+20)
//        goback.frame.size=CGSize(width :self.view.frame.height/5,height:0)
//        goback.frame.size.height = goback.frame.width/1.8
//        cancel.frame = goback.frame
//        cancel.frame.origin.x = self.view.frame.width-(Pic.frame.origin.x+cancel.frame.width)
//            goback.backgroundColor = .green
//        cancel.backgroundColor = .green
//        goback.setTitle("Back", for: .normal)
//        cancel.setTitle("Cancel", for: .normal)
//        goback.addTarget(self, action: #selector(back), for: .touchUpInside)
//        cancel.addTarget(self, action: #selector(cancels), for: .touchUpInside)
//        
//        if makesure.isDescendant(of: self.view) {
//           
//        } else {
//            self.view.addSubview(makesure)
//            makesure.addSubview(caution)
//            makesure.addSubview(descriptions)
//            makesure.addSubview(goback)
//            makesure.addSubview(cancel)
//        }
//
//        
//        
////        navigationController?.popViewController(animated: true)
////        dismiss(animated: true,completion:nil)
//        
    }
    func cancels(){
    
        if makesure.isDescendant(of: self.view) {
            makesure.removeFromSuperview()
        }
    }
    
    func back(_ ss: UIAlertAction){
    navigationController?.popViewController(animated: true);
        dismiss(animated: true, completion: nil);
    }
    func estimatedsize(text  : String, sizee: Int) -> CGRect{
        let size  = CGSize(width: Pic.frame.width, height: 1000)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string :  text).boundingRect(with: size, options: option, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: CGFloat(sizee))], context: nil)
    }
    func pickerdestroyer(_ sender : UITapGestureRecognizer) {
        space.removeFromSuperview()
        
        UIView.animate(withDuration: 0.5){
        self.openpicker.alpha = 0
        
        }
        space.removeFromSuperview()
        
               let hhd = opentime.center
        switch openpicker.int{
        case 1:
            
            timeop = (Int(timeforsave2.string(from:openpicker.date))!*60*60+Int(timeforsave1.string(from:openpicker.date))!*60)
        opentime.text = timeformatter1.string(from: openpicker.date)
            break
        case 2:
            closetime.text = timeformatter1.string(from: openpicker.date)
            timecl = (Int(timeforsave2.string(from:openpicker.date))!*60*60+Int(timeforsave1.string(from:openpicker.date))!*60)
            break
        default:
            break
        }
        
        
     
        if openpicker.isDescendant(of: self.view){
            openpicker.removeFromSuperview()
            openpicker.int = 0
        }
        
        
    }
    var changespic = false
    func Save(){
      var saveispass = true
        errorrespond = []

        let storageref = FIRStorage.storage().reference().child("shop_images").child(shopnamefield.text).child("activity.jpg")
        if (Pic.currentImage != nil) && changespic == true {
            var setting = true
            var quality = 1.0 as CGFloat
            while setting == true {
                if let upload = UIImageJPEGRepresentation(Pic.currentImage!, quality){
                    
                    let imgData: NSData = NSData(data: upload)
                    
                    let imageSize: Int = imgData.length
                    print("size of image in KB: %f ", imageSize / 1024)
                    if (imageSize / 1024) < 1000 {
                        setting = false;
                        storageref.put(upload,metadata : nil,completion:nil)
                    }
                    quality=quality-0.2
                }
            }
            
            } else {
            saveispass = false
            //ERROR NO PIC IS UPLOADED
            errorrespond.append("ERROR NO PIC IS UPLOADED")
        }
        //
        //
        //
        ///////////////////////////////////////////////////////////////////////////////////////
        //
        //
        //
        var error = false
        for im in 0...imagesforotherview.count-1{
            let imsave = imagesforotherview[im]
        if (imsave.image != nil)  {
            var setting = true
            var quality = 1.0 as CGFloat
            while setting == true && saveispass == true {
                if let upload = UIImageJPEGRepresentation(imsave.image!, quality){
                    
                    let imgData: NSData = NSData(data: upload)
                    
                    let imageSize: Int = imgData.length
                    print("size of image in KB: %f ", imageSize / 1024)
                    if (imageSize / 1024) < 1000 {
                        setting = false;
                        
                          let storageref = FIRStorage.storage().reference().child("shop_images").child(shopnamefield.text)
                        var uploadsd = FIRStorageReference()
                        if im == 0 {
                         uploadsd = storageref.child("shopFront.jpg")
                        } else
                            if im == 1 {
                                 uploadsd = storageref.child("shopIn.jpg")
                        } else
                                if im == 2 {
                                     uploadsd = storageref.child("shopCool.jpg")
                        }
                        
                        
                      uploadsd.put(upload,metadata : nil,completion:nil)
                        
                        
                        
                    }
                    quality=quality-0.2
                }
            }
            
        } else if error == false{
            saveispass = false
            error = true
            //ERROR NO PIC IS UPLOADED
            errorrespond.append("ERROR NO GALERIES")
        }
        }
        error = false
        for im in 0...imagesforiteminmenuview.count-1{
            let imsave = imagesforiteminmenuview[im]
            if (imsave.image != nil)  {
                var setting = true
                var quality = 1.0 as CGFloat
                while setting == true && saveispass == true
                {
                    if let upload = UIImageJPEGRepresentation(imsave.image!, quality){
                        
                        let imgData: NSData = NSData(data: upload)
                        
                        let imageSize: Int = imgData.length
                        print("size of image in KB: %f ", imageSize / 1024)
                        if (imageSize / 1024) < 1000 {
                            setting = false;
                             let storageref = FIRStorage.storage().reference().child("shop_images").child(shopnamefield.text)
                            if saveispass == true {
                                var uploadsd = FIRStorageReference()
                                if im == 0 {
                                    uploadsd = storageref.child(String(item1[0].text+".jpg"))
                                } else
                                    if im == 1 {
                                        uploadsd = storageref.child(String(item1[1].text+".jpg"))
                                }
                                //
                                uploadsd.put(upload,metadata : nil,completion:nil)
                                //
                            }
                            
                        }
                        quality=quality-0.2
                    }
                }
                
            } else if error == false{
                saveispass = false
                error = true
                //ERROR NO PIC IS UPLOADED
                errorrespond.append("ERROR NO ITEM")
            }
        }
        
            if (imagesformenuview.image != nil)  {
                var setting = true
                var quality = 1.0 as CGFloat
                while setting == true && saveispass == true {
                    if let upload = UIImageJPEGRepresentation(self.imagesformenuview.image!, quality){
                        
                        let imgData: NSData = NSData(data: upload)
                        
                        let imageSize: Int = imgData.length
                        print("size of image in KB: %f ", imageSize / 1024)
                        if (imageSize / 1024) < 1000 {
                            setting = false;
                            if saveispass == true {
                                //
                                 let storageref = FIRStorage.storage().reference().child("shop_images").child(shopnamefield.text).child("Menu.jpg")
                                storageref.put(upload,metadata : nil,completion:nil)
                                //
                            }
                            
                        }
                        quality=quality-0.2
                    }
                }
                
            } else {
                saveispass = false
                //ERROR NO PIC IS UPLOADED
                errorrespond.append("ERROR NO MENU")
            }
        
        
        
    let refShop = FIRDatabase.database().reference().child("shops")
let sav = refShop.child(shopnamefield.text)
        print(shopnamefield.text)
let uid = FIRAuth.auth()?.currentUser?.uid
        
        for county in shops{
                print(county.shop!,"fuckinghell")
          if shopnamefield.text == county.shop! || shopnamefield.text == "" {
        saveispass = false
            //ERROR DONT HAVE SHOPNAME OR DUPLICATED
            errorrespond.append("ERROR DONT HAVE SHOPNAME OR DUPLICATED")
            }
        
        }
        if descriptionfield.text == "" || descriptionfield.text == "description"{
            saveispass = false
            //ERROR DONT HAVE DESCRIPTION
            errorrespond.append("ERROR DONT HAVE DESCRIPTION")
        }
        if saveispass == true {
        let valuesShop = [
            "owner": uid!,
            "TimeClosed" : timeop,
                            "TimeOpened" : timecl,
                            "comments" : [] ,
                            "descripTion" : descriptionfield.text,
                            "favorite" : [],
                            "heightImage" : Pic.currentImage?.size.height,
                            "highlight" : "",
                            "type" : icontype,
                            "widthImage" : Pic.currentImage?.size.width,
                            "heightImageItem1" : imagesforiteminmenuview[0].image?.size.height,
                            "widthImageItem1" : imagesforiteminmenuview[0].image?.size.width,
                            "heightImageItem2" : imagesforiteminmenuview[1].image?.size.height,
                            "widthImageItem2" : imagesforiteminmenuview[1].image?.size.width,
                            "menuItem1" : item1[0].text,
                            "menuItem2" : item1[1].text,
                            "heightmenu":imagesformenuview.image?.size.height,
                            "widthmenu":imagesformenuview.image?.size.height
                            
                            
                            
                            ] as [String : Any]
        sav.updateChildValues(valuesShop)
        
        navigationController?.popViewController(animated: true)
//            self.dismiss(animated: true, completion: nil)
        } else if saveispass == false {
            
            
            //
            //
            //
            //
            errorresult = ""
            for s in errorrespond{
            errorresult = errorresult+"\n "+s
                
            
            }
            print(errorresult)
            let alertController = UIAlertController(title: errorresult, message: "", preferredStyle: .alert)
            
            
            let cancelaction = UIAlertAction(title:"Ok",style : .default,handler : nil)
           
            alertController.addAction(cancelaction)
            present(alertController, animated: true, completion: nil)
            //
            //
            //
            //
            //
            //
            //
            
            //
            //
            //
            //
            //
            
            
            
            
            
        
        
        }
        print("dasgafgdfgssdgsdfgsfdgdfg")
    }
    let errorcover = UIView()
    var errorrespond = [String()]
    var errorresult = String()
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        positioning()
    }
    
}
extension UITextView {
    func roundcorner(){
        
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
    
}
extension UITextField {
    func roundcorner(){
        
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
    
}
class Mygesture : UITapGestureRecognizer {
    
    var int  = Int()
}
class gees : UITapGestureRecognizer {
    }

class Shop: NSObject {
    var shop:String?
    var longitude:NSNumber?
    var latitude:NSNumber?
    var descripTion:String?
    var TimeClosed:NSNumber?
    var TimeOpened:NSNumber?
    var type:String?
    var widthImage:NSNumber?
    var heightImage:NSNumber?
    var distance:Double?
    var favorite:[String:Bool]?
    var rating:[String:NSNumber]?
    var comments:[String:Any]?
    var isFavorite:Bool?
    var isRate:Bool?
    var rateTotal:NSNumber?
    var lastComment:String?
    var lastUserComment:String?
    var lastUserUidComment:String?
    var lastCommentTimeStamp:NSNumber?
    var highlight:String?
    var owner:String?
    var phone:String?
    
    var ad:String?
    var require:String?
    var expire:NSNumber?
    var used:[String:Int]?
    var isUserUsed:Bool?
    var serial:Int?
    var heightmenu : NSNumber?
    var widthmenu : NSNumber?
    
    
    
    var heightImageItem1:NSNumber?
    var heightImageItem2:NSNumber?
    var menuItem1:String?
    var menuItem2:String?
    var widthImageItem1:NSNumber?
    var widthImageItem2:NSNumber?
    var priceItem1:String?
    var priceItem2:String?
}
extension UIButton {
    func roundcorner() {
    self.layer.cornerRadius = self.frame.height/2
    
        self.layer.masksToBounds = true
    }

}
extension UILabel {
    func roundcorner() {
        self.layer.cornerRadius = self.frame.height/5
        
        self.layer.masksToBounds = true
    }
    
}
