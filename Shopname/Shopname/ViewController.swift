//
//  ViewController.swift
//  Shopname
//
//  Created by MAC_OS on 2/18/2560 BE.
//  Copyright © 2560 MAC_OS. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class ViewController: UITableViewController {
let celliden =  "celltable"
 
    public var shops = [Shop]()
    func handleLogout() {
        do {
            try FIRAuth.auth()?.signOut()
        } catch let err {
            print(err)
        }
        let vc = LoginController()
        self.present(vc, animated: true, completion: nil)
    }
    func isUserLogedin() {
        if FIRAuth.auth()?.currentUser == nil {
            handleLogout()
            return
        }
    }
    func fetchShopNcoupon(){
        
        let uid = FIRAuth.auth()?.currentUser?.uid
        let shopRef = FIRDatabase.database().reference().child("shops")
        shopRef.observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String:AnyObject]{
                let shop = Shop()
                shop.setValuesForKeys(dictionary)
                shop.shop = snapshot.key
                if shop.owner == uid {
                self.shops.append(shop)
                }
                DispatchQueue.main.async {
                    self.tableView?.reloadData()
                }
            }
            
        }, withCancel: nil)
        
        shopRef.observe(.childChanged, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String:AnyObject] {
                let shop = Shop()
                shop.setValuesForKeys(dictionary)
                shop.shop = snapshot.key
                
                if let favorite = shop.favorite {
                    print(favorite)
                    for (xUid,_) in favorite {
                        if xUid == uid {
                            shop.isFavorite = true
                        }
                    }
                }
                if let rate = shop.rating {
                    for (userRated,_) in rate {
                        if userRated == uid {
                            shop.isRate = true
                        }
                    }
                }
                var count:Int = 0
                for oldShop in self.shops {
                    if oldShop.shop == shop.shop {
                        DispatchQueue.main.async {
                            self.shops.remove(at: count)
                            self.shops.insert(shop, at: count)
                            self.tableView?.reloadData()
                        }
                        return
                    }
                    count = count + 1
                }
                print("dont' return")
                
            }
        }, withCancel: nil)
        shopRef.observe(.childRemoved, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String:AnyObject] {
                let shop = Shop()
                shop.setValuesForKeys(dictionary)
                shop.shop = snapshot.key
                
                if let favorite = shop.favorite {
                    print(favorite)
                    for (xUid,_) in favorite {
                        if xUid == uid {
                            shop.isFavorite = true
                        }
                    }
                }
                if let rate = shop.rating {
                    for (userRated,_) in rate {
                        if userRated == uid {
                            shop.isRate = true
                        }
                    }
                }
                var count:Int = 0
                for oldShop in self.shops {
                    if oldShop.shop == shop.shop {
                        DispatchQueue.main.async {
                            self.shops.remove(at: count)
                            self.tableView?.reloadData()
                        }
                        return
                    }
                    count = count + 1
                }
                print("dont' return")
                
            }
        }, withCancel: nil)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
       tableView.reloadData()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        UILabel.appearance().font = UIFont(name:"Oxygen-Light",size: 38)
        isUserLogedin()
       
        fetchShopNcoupon()
        tableView.reloadData()
//        let navigationBar = UINavigationBar(frame: CGRect(x:0,y: 20,width: view.frame.size.width, height:44));
 
        let navigationItem = UINavigationItem()
        navigationItem.title = "Shop List"
//        let leftButton =  UIBarButtonItem(title: "Setting", style:   UIBarButtonItemStyle.plain, target: self, action: Selector(("leftClicked:")))
        let rightButton = UIBarButtonItem(title: "Create", style: UIBarButtonItemStyle.plain, target: self, action: #selector(gotocreate))
        
       
//       self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.rightBarButtonItem = rightButton
     
        
//        self.view.addSubview(navigationBar)
       
        tableView.register(TableCell.self,forCellReuseIdentifier: celliden)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shops.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
                       
                       
           
        
                let shop = shops[indexPath.item]
        let cell = tableView.dequeueReusableCell(withIdentifier: celliden, for: indexPath) as! TableCell
        cell.Shopname.text = shops[indexPath.item].shop
        cell.Shoptype.image = nil
        let activity_image = FIRStorage.storage().reference(withPath: "shop_images/\(shop.shop!)/activity.jpg")
        if let cachedImage = imageCache.object(forKey: activity_image as AnyObject) as? UIImage {
            cell.Shoptype.image = cachedImage
        } else {
            activity_image.data(withMaxSize: 1000 * 1000, completion: { (data, err) in
                if err != nil {
                    print(err!)
                    return
                }
                
                cell.Shoptype.alpha = 0
                cell.Shoptype.image = UIImage(data: data!)
                
                UIView.animate(withDuration: 0.5, animations: {
                    cell.Shoptype.alpha = 1
                }, completion: nil)
                imageCache.setObject(UIImage(data: data!)!, forKey: activity_image as AnyObject)
            })
        }
        
        cell.layout()
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func gotocreate(){
        let sss = Shopsetting()
        sss.shops = shops
    self.navigationController?.pushViewController(sss, animated: true)
    }
     override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let usesshop = shops[indexPath.item]
        let vc = DetailViewController()
        vc.shop = usesshop
        self.present(vc, animated: true, completion: nil)
        
      
//        let coupon  = Couponedit()
//        coupon.shops = usesshop
//       
//         self.navigationController?.pushViewController(coupon, animated: true)
    }
         func estimatedsize(text  : String, sizee: CGFloat, font: String, width: CGFloat) -> CGRect{
                let CGsize  = CGSize(width: width, height: 1000)
                let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                return NSString(string :  text).boundingRect(with: CGsize, options: option, attributes: [NSFontAttributeName: UIFont(name: font, size: sizee)!], context: nil)
                
            }
}

class TableCell: UITableViewCell {
    var Shoptype = UIImageView()
    var Shopname = UILabel()
    var Couponimage = UIImageView()
    var Couponcount = UILabel()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        Shopname.text =  "ShopName"
        Couponcount.text = "  "+"20"
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        
    }
    
     func layout() {
       
        Shoptype.frame = CGRect(x: self.frame.height/14,y:0,width:self.frame.height - self.frame.height/7, height: self.frame.height - self.frame.height/7 )
        Shoptype.center.y = self.frame.height/2
        Shoptype.image = UIImage(named:"images.png")
        
//        Shopname.font = UIFont.systemFont(ofSize: 18)
        Shopname.frame = CGRect(x:self.frame.height,y: self.frame.height/14,width: self.frame.width - self.frame.height,height: self.frame.height*2/5)
        Couponimage.frame = CGRect(x: Shopname.frame.origin.x ,y:Shopname.frame.origin.y+Shopname.frame.height,width:self.frame.height*2/5,height:self.frame.height*2/5)
        Couponimage.image = UIImage(named:"images.png")
        
        
        Couponcount.frame = CGRect(x: Shopname.frame.origin.x+Couponimage.frame.width,y:Shopname.frame.origin.y+Shopname.frame.height,width:self.frame.height*2/5,height:self.frame.height*2/5)
                self.contentView.addSubview(Shoptype)
        self.contentView.addSubview(Shopname)
        self.contentView.addSubview(Couponimage)
        self.contentView.addSubview(Couponcount)
        
        
    }
}
